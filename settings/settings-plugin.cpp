/*
  *
  * This file is a part of Paper Settings.
  * Paper Settings is the Settings app to manage Paper Desktop settings
  * Copyright 2020 CuboCore Group
  *
  *
  *
  * This program is free software; you can redistribute it and/or modify
  * it under the terms of the GNU General Public License as published by
  * the Free Software Foundation; either version 3 of the License, or
  * (at your option) any later version.
  *
  *
  *
  * This program is distributed in the hope that it will be useful,
  * but WITHOUT ANY WARRANTY; without even the implied warranty of
  * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  * GNU General Public License for more details.
  *
  *
  *
  * You should have received a copy of the GNU General Public License
  * along with this program; if not, write to the Free Software
  * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
  * MA 02110-1301, USA.
  *
  */

#include <QDebug>
#include <QFileDialog>
#include <QToolButton>

#include <paper-config.h>
#include <paperde/papersettings.h>

#include "settings.h"
#include "ui_settings.h"

#include <cprime/cplugininterface.h>

void Paper::SettingsUI::loadPluginPaths()
{
    auto pluginPathsList = settingsWidget->findChild<QListWidget *>("pluginPathsList");

    m_pluginsFolders.clear();
    m_pluginsFolders << PluginPath << "/usr/lib/coreapps/plugins/";

    pluginPathsList->clear();
    for (auto &path : m_pluginsFolders) {
        auto item = new QListWidgetItem(QIcon::fromTheme("folder"), path);
        item->setFlags(Qt::NoItemFlags);

        pluginPathsList->addItem(item);
    }

    QStringList others = shellSett->value("Plugins/PluginPaths");

    others.removeAll(""); // Remove empty paths

    if (not others.isEmpty()) {
        m_pluginsFolders << others;

        for (auto &path : others) {
            auto item = new QListWidgetItem(QIcon::fromTheme("folder"), path);
            item->setFlags(Qt::ItemIsEnabled | Qt::ItemIsSelectable);

            pluginPathsList->addItem(item);
        }
    }
}

void Paper::SettingsUI::addPluginPath()
{
    auto *pluginPathsList = settingsWidget->findChild<QListWidget *>("pluginPathsList");
    QString path = QFileDialog::getExistingDirectory(this, "Select plugin location");

    if (path.isNull() || path.isEmpty()) {
        return;
    }

    m_pluginsFolders << path;

    auto *item = new QListWidgetItem(QIcon::fromTheme("folder"), path);

    item->setFlags(Qt::ItemIsEnabled | Qt::ItemIsSelectable);
    pluginPathsList->addItem(item);

    QStringList pPaths = shellSett->value("Plugins/PluginPaths");

    pPaths << path;
    shellSett->setValue("Plugins/PluginPaths", pPaths);

    loadPlugins();
}

void Paper::SettingsUI::removePluginPath()
{
    auto *pluginPathsList = settingsWidget->findChild<QListWidget *>("pluginPathsList");

    if (not pluginPathsList->selectedItems().count()) {
        return;
    }

    QString path = pluginPathsList->selectedItems().at(0)->text();

    delete pluginPathsList->selectedItems().at(0);

    m_pluginsFolders.removeAll(path);
    QStringList pPaths = shellSett->value("Plugins/PluginPaths");

    pPaths.removeAll(path);
    shellSett->setValue("Plugins/PluginPaths", pPaths);

    loadPlugins();
}

void Paper::SettingsUI::loadPlugins()
{
    m_selectedPlugins = shellSett->value("Plugins/SelectedPlugins");

    Q_FOREACH (QString path, m_pluginsFolders) {
        QDir pluginInfoDir(path);
        Q_FOREACH (QFileInfo pluginSoInfo, pluginInfoDir.entryInfoList({"*.so"}, QDir::Files)) {
            if (plugins.contains(pluginSoInfo.absoluteFilePath())) {
                continue;
            }

            QPluginLoader loader(pluginSoInfo.absoluteFilePath());
            auto pObject = loader.instance();

            if (pObject) {
                WidgetsInterface *plugin = qobject_cast<WidgetsInterface *>(pObject);
                if (plugin) {
                    plugins[pluginSoInfo.absoluteFilePath()]
                        = PluginInfo{plugin->name(), QVersionNumber::fromString(plugin->version())};
                }
            }
        }
    }

    sortCheckedAtFirst();
}

void Paper::SettingsUI::pluginSelectionChanged(QListWidgetItem *)
{
    auto *pluginList = settingsWidget->findChild<QListWidget *>("pluginsList");

    QStringList selected;

    for (int i = 0; i < pluginList->count(); i++) {
        QListWidgetItem *item = pluginList->item(i);
        if (item->checkState() == Qt::Checked) {
            selected << item->data(Qt::UserRole + 1).toString();
        }
    }

    shellSett->setValue("Plugins/SelectedPlugins", selected);
    m_selectedPlugins = selected;

    sortCheckedAtFirst();
}

void Paper::SettingsUI::movePluginUp()
{
    auto *pluginList = settingsWidget->findChild<QListWidget *>("pluginsList");
    QList<QListWidgetItem *> selList = pluginList->selectedItems();

    if (not selList.count()) {
        return;
    }

    QListWidgetItem *item = selList.at(0);

    if (item->checkState() == Qt::Checked) {
        QStringList selPlugins = shellSett->value("Plugins/SelectedPlugins");
        QString plugin = item->data(Qt::UserRole + 1).toString();
        int idx = selPlugins.indexOf(plugin);
        if (idx > 0) {
            selPlugins.move(idx, idx - 1);
            m_selectedPlugins = selPlugins;
            shellSett->setValue("Plugins/SelectedPlugins", selPlugins);

            loadPlugins();
        }
    }
}

void Paper::SettingsUI::movePluginDown()
{
    auto *pluginList = settingsWidget->findChild<QListWidget *>("pluginsList");
    QList<QListWidgetItem *> selList = pluginList->selectedItems();

    if (not selList.count()) {
        return;
    }

    QListWidgetItem *item = selList.at(0);

    if (item->checkState() == Qt::Checked) {
        QStringList selPlugins = shellSett->value("Plugins/SelectedPlugins");
        QString plugin = item->data(Qt::UserRole + 1).toString();
        int idx = selPlugins.indexOf(plugin);
        if ((idx > 0) and (idx < selPlugins.count() - 1)) {
            selPlugins.move(idx, idx + 1);
            m_selectedPlugins = selPlugins;
            shellSett->setValue("Plugins/SelectedPlugins", selPlugins);

            loadPlugins();
        }
    }
}

void Paper::SettingsUI::sortCheckedAtFirst()
{
    auto *pluginList = settingsWidget->findChild<QListWidget *>("pluginsList");

    pluginList->clear();

    Q_FOREACH (auto plugin, m_selectedPlugins) {
        if (not plugins.contains(plugin)) {
            continue;
        }

        auto pi = plugins[plugin];

        auto item = new QListWidgetItem(QIcon::fromTheme("plugin"),
                                        pi.name + " " + pi.version.toString());
        //		item->setData( Qt::UserRole, pi.path );
        item->setData(Qt::UserRole + 1, plugin);
        item->setFlags(Qt::ItemIsEnabled | Qt::ItemIsSelectable | Qt::ItemIsUserCheckable);
        item->setCheckState(Qt::Checked);

        pluginList->addItem(item);
    }

    Q_FOREACH (auto plugin, plugins.keys()) {
        if (m_selectedPlugins.contains(plugin)) {
            continue;
        }

        auto pi = plugins[plugin];

        auto item = new QListWidgetItem(QIcon::fromTheme("plugin"),
                                        pi.name + " " + pi.version.toString());
        //		item->setData( Qt::UserRole, pi.path );
        item->setData(Qt::UserRole + 1, plugin);
        item->setFlags(Qt::ItemIsEnabled | Qt::ItemIsSelectable | Qt::ItemIsUserCheckable);
        item->setCheckState(Qt::Unchecked);

        pluginList->addItem(item);
    }
}
