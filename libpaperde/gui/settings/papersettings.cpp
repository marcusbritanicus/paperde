/*
  *
  * This file is a part of libpaperde.
  * Library for handling various services related to Paper Desktop
  * Copyright 2020 CuboCore Group
  *
  *
  *
  * This file was originally a part of CoreApps project (https://gitlab.com/cubocore/coreapps)
  * Suitable modifications have been done to meet the needs of Paper Desktop.
  *
  *
  *
  * This program is free software; you can redistribute it and/or modify
  * it under the terms of the GNU General Public License as published by
  * the Free Software Foundation; either version 3 of the License, or
  * (at your option) any later version.
  *
  *
  *
  * This program is distributed in the hope that it will be useful,
  * but WITHOUT ANY WARRANTY; without even the implied warranty of
  * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
  * GNU General Public License for more details.
  *
  *
  *
  * You should have received a copy of the GNU General Public License
  * along with this program; if not, write to the Free Software
  * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
  * MA 02110-1301, USA.
  *
  */

#include <QDebug>
#include <QVariant>

#include <cprime/themefunc.h>
#include <time.h>

#include "paper-config.h"
#include "papersettings.h"

Paper::Settings::Settings(QString app)
{
    system = new QSettings(ConfigPath + app + ".conf", QSettings::IniFormat);
    user = new QSettings("paperde", app);

    QFileSystemWatcher* fsw = new QFileSystemWatcher(this);

    fsw->addPath(system->fileName());
    fsw->addPath(user->fileName());
    connect(fsw, &QFileSystemWatcher::fileChanged, [=](const QString& file) {
        /* User config */
        if (file.startsWith("/home")) {
            user->sync();
            emitSettings(user);
        }

        else {
            system->sync();
            emitSettings(system);
        }

        /* File deleted and created again: add it to the watcher */
        if (not fsw->files().contains(file) and QFile::exists(file)) {
            fsw->addPath(file);
        }
    });
}

QVariant Paper::Settings::rawValue(const QString& key)
{
    /* SafetyNet: if the @system does not contain this key, then the setting is not available */
    /* However, session data will not be available in @system, so ignore the 'Session/' stuff */
    if (not system->contains(key) and not key.startsWith("Session/")) {
        return QVariant();
    }

    /* In all cases this will be the same */
    if (key == "FormFactor") {
        return CPrime::ThemeFunc::getFormFactor();
    }

    QVariant value = (user->contains(key) ? user->value(key) : system->value(key));

    /* TouchMode: As seen by the oser */
    if (key == "Personalization/UIMode") {
        QString xkey("Personalization/AutoDetect");
        bool isAutoDetect = (user->contains(xkey) ? user->value(xkey) : system->value(xkey)).toBool();
        if (isAutoDetect) {
            int formFactor = CPrime::ThemeFunc::getFormFactor();
            int uiMode;
            if (formFactor == 2) {
                uiMode = 2;
            } else if ((formFactor == 1) && (CPrime::ThemeFunc::getTouchMode() == 1)) {
                uiMode = 1;
            } else {
                uiMode = 0;
            }

            return QVariant(uiMode);
        }

        return value;
    }

    /* Convert ~/ to QDir::homePath() */
    if (value.typeId() == QMetaType::QString) {
        QString strValue = value.toString();
        if (strValue.startsWith("~/") or strValue == "~") {
            strValue = strValue.replace("~", QDir::homePath());
        }

        return QVariant(strValue);
    }

    return value;
}

Paper::Settings::PaperProxy Paper::Settings::value(const QString& key)
{
    QVariant value = rawValue(key);

    return PaperProxy{value};
}

void Paper::Settings::setValue(const QString& key, QVariant value)
{
    if (not system->contains(key) and not key.startsWith("Session/")) {
        qWarning() << "Unknown key:" << key;
        qWarning() << "Report this incident to the developers.";

        return;
    }

    user->setValue(key, value);
    user->sync();
}

void Paper::Settings::emitSettings(QSettings* sett)
{
    /* If the global settings changed */
    if (sett == system) {
        qDebug() << "Global settings have changed. This generally does not happen. "
                    "If your app was upgraded, then it's strongly recommended that "
                    "you restart your app. Otherwise you may face compatibility issues.";

        return;
    }

    /* We are bothered only about changes to system settings */
    SettingsMap newMap;

    for (QString key : user->allKeys()) {
        /* Store the (key, vaule) pair */
        newMap[key] = user->value(key);

        /* Not important for a running app, is it? */
        if (key.startsWith("Session/")) {
            continue;
        }

        /* If the key isn't in the system config, don't alert the user */
        if (not system->contains(key)) {
            continue;
        }

        /* If the value is same as the old one, nothing to do */
        if (userMap[key] == user->value(key)) {
            continue;
        }

        /* New key, or value has changed */
        emit settingChanged(key, newMap[key]);
    }

    /* A key may have been removed from the config file. */
    /* Check and return system config value */
    for (QString key : userMap.keys()) {
        if (not newMap.contains(key)) {
            if (system->contains(key)) {
                emit settingChanged(key, system->value(key));
            }
        }
    }

    /* Clear the old map, and store the new map in it. */
    userMap.clear();
    userMap = newMap;
}
