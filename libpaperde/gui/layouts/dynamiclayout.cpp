/*
  *
  * This file is a part of libpaperde.
  * Library for handling various services related to Paper Desktop
  * Copyright 2020 CuboCore Group
  *
  *
  *
  * This file was originally a part of SmartVerticalFlowLayout project (https://github.com/VaysseB/SmartVerticalFlowLayout)
  * Suitable modifications have been done to meet the needs of Paper Desktop.
  *
  *
  *
  * This program is free software; you can redistribute it and/or modify
  * it under the terms of the GNU General Public License as published by
  * the Free Software Foundation; either version 3 of the License, or
  * (at your option) any later version.
  *
  *
  *
  * This program is distributed in the hope that it will be useful,
  * but WITHOUT ANY WARRANTY; without even the implied warranty of
  * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
  * GNU General Public License for more details.
  *
  *
  *
  * You should have received a copy of the GNU General Public License
  * along with this program; if not, write to the Free Software
  * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
  * MA 02110-1301, USA.
  *
  */

#include "dynamiclayout.h"
#include <QWidget>
#include <QtMath>

Paper::DynamicLayout::DynamicLayout(QWidget *parent)
    : QLayout(parent)
    , m_hSpace(-1)
    , m_vSpace(-1)
    , m_maxRowCount(-1)
    , m_isLayoutModified(false)
{
    setContentsMargins(-1, -1, -1, -1);
}

Paper::DynamicLayout::~DynamicLayout()
{
    qDeleteAll(m_items);
}

int Paper::DynamicLayout::horizontalSpacing() const
{
    if (m_hSpace >= 0) {
        return m_hSpace;
    }
    return smartSpacing(QStyle::PM_LayoutHorizontalSpacing);
}

int Paper::DynamicLayout::verticalSpacing() const
{
    if (m_vSpace >= 0) {
        return m_vSpace;
    }
    return smartSpacing(QStyle::PM_LayoutVerticalSpacing);
}

void Paper::DynamicLayout::setHorizontalSpacing(int space)
{
    m_isLayoutModified |= (m_hSpace != space);
    m_hSpace = space;
    doLayout(geometry());
}

void Paper::DynamicLayout::setVerticalSpacing(int space)
{
    m_isLayoutModified |= (m_vSpace != space);
    m_vSpace = space;
    doLayout(geometry());
}

int Paper::DynamicLayout::maxRowCount() const
{
    return m_maxRowCount;
}

void Paper::DynamicLayout::setMaxRowCount(int count)
{
    m_isLayoutModified |= (m_maxRowCount != count);
    m_maxRowCount = count;
    doLayout(geometry());
}

int Paper::DynamicLayout::count() const
{
    return m_items.size();
}

void Paper::DynamicLayout::addItem(QLayoutItem *item)
{
    m_items.append(item);
}

QLayoutItem *Paper::DynamicLayout::itemAt(int index) const
{
    return m_items.value(index);
}

QLayoutItem *Paper::DynamicLayout::takeAt(int index)
{
    m_isLayoutModified = true;
    QLayoutItem *item = m_items.takeAt(index);

    doLayout(geometry());
    return item;
}

void Paper::DynamicLayout::setAlignment(Qt::Alignment align)
{
    m_isLayoutModified = (alignment() != align);
    QLayout::setAlignment(align);
    doLayout(geometry());
}

Qt::Orientations Paper::DynamicLayout::expandingDirections() const
{
    if ((alignment() == 0) || (alignment() == Qt::AlignJustify)) {
        return Qt::Horizontal;
    }
    return Qt::Vertical;
}

void Paper::DynamicLayout::setGeometry(const QRect &rect)
{
    m_isLayoutModified |= (rect != geometry());
    doLayout(rect);
    QLayout::setGeometry(rect);
}

bool Paper::DynamicLayout::hasHeightForWidth() const
{
    return true;
}

int Paper::DynamicLayout::heightForWidth(int width)
{
    m_isLayoutModified = true;
    doLayout(QRect(0, 0, width, 1));
    return m_structureGeometry.height();
}

QSize Paper::DynamicLayout::sizeHint() const
{
    return m_structureGeometry.size();
}

QSize Paper::DynamicLayout::minimumSize() const
{
    return sizeHint();
}

void Paper::DynamicLayout::updateLayout()
{
    m_isLayoutModified = true;
    doLayout(geometry());
}

void Paper::DynamicLayout::doLayout(const QRect &rect)
{
    if (!m_isLayoutModified) {
        return;
    }

    int left, top, right, bottom;

    getContentsMargins(&left, &top, &right, &bottom);
    QRect effectiveRect = rect.adjusted(+left, +top, -right, -bottom);
    int x = effectiveRect.x();

    // update structure (ordering line by lines)
    m_structure.clear();
    QLayoutItem *item = 0;
    QList<QLayoutItem *> rowItems;

    Q_FOREACH (item, m_items) {
        QSize itemSize = item->sizeHint();

        // if the item is over the line limit OR limit reach per row
        if ((x + itemSize.width() > effectiveRect.right() + 1)
            || ((m_maxRowCount > 0) && (rowItems.count() >= m_maxRowCount))) {
            // if the line isn't empty => we add to the structure
            // and reset the row
            if (!rowItems.isEmpty()) {
                m_structure.append(rowItems);
                x = effectiveRect.x();
            }
            rowItems.clear();
        }

        rowItems.append(item);
        x += itemSize.width() + horizontalSpacing();
    }

    if (!rowItems.isEmpty()) {
        m_structure.append(rowItems);
    }

    int y = effectiveRect.y();
    // update items position
    QRect layoutGeometry
        = QRect(0, 0, 1, 1); // width and height msut be positive ! (otherwise, infinite loop)

    Q_FOREACH (rowItems, m_structure) {
        // reset horizontal position
        x = effectiveRect.x();

        // calculating row height and row width
        int rowHeight = 0;
        int rowWidth = 0;
        Q_FOREACH (item, rowItems) {
            QSize itemSize = item->sizeHint();
            rowWidth += itemSize.width();
            rowHeight = qMax(rowHeight, itemSize.height());
        }

        // placing X coordinates
        int hSpaceTot = horizontalSpacing() * (rowItems.count() - 1);
        int freeSpace = effectiveRect.width() - rowWidth - hSpaceTot;
        // if stretch content => nothing to do (free space is ignored)
        // if no strect and center
        if ((alignment() & Qt::AlignHCenter)) {
            // shift to align center
            x += freeSpace / 2;
        }
        // if no strect and left
        else if ((alignment() & Qt::AlignLeft)) {
            // no shift X (already align to the left)
        }
        // if no strect and right
        else if ((alignment() & Qt::AlignRight)) {
            // shift to align the right
            x += freeSpace;
        }

        qreal cumulStrecthWidth = x;

        // calculating space between elements

        // setting position of row elements
        Q_FOREACH (item, rowItems) {
            QSize itemSize = item->sizeHint();

            QRect itemGeometry;

            // if no strect and align
            if ((alignment() & Qt::AlignHCenter) || (alignment() & Qt::AlignLeft)
                || (alignment() & Qt::AlignRight)) {
                itemGeometry = QRect(x, y, itemSize.width(), rowHeight);

                // moving to next element position
                x += itemSize.width() + horizontalSpacing();
            }
            // if stretch content
            else {
                // getting true width depending on the pourcentage of width taken by the item in the row
                qreal strecthOptimalWidth = (effectiveRect.width() - hSpaceTot)
                                            * ((qreal) itemSize.width()) / ((qreal) rowWidth);

                itemGeometry = QRect(x, y, qFloor(strecthOptimalWidth), rowHeight);

                qreal xIncr = strecthOptimalWidth + horizontalSpacing();
                cumulStrecthWidth += xIncr;
                x = qFloor(cumulStrecthWidth);
            }

            // update geometry of item
            item->setGeometry(itemGeometry);

            // update internal geometry
            layoutGeometry = layoutGeometry.united(itemGeometry);
        }

        y += rowHeight + verticalSpacing();
    }

    layoutGeometry.setHeight(layoutGeometry.height() + bottom);
    layoutGeometry.setWidth(layoutGeometry.width() + right);
    m_structureGeometry = layoutGeometry;

    m_isLayoutModified = false;
}

int Paper::DynamicLayout::smartSpacing(QStyle::PixelMetric pm) const
{
    QObject *parent = this->parent();
    QLayout *parentLayout = nullptr;

    int space = 0;

    if (parent && parent->isWidgetType()) {
        auto *pw = dynamic_cast<QWidget *>(parent);
        space = pw->style()->pixelMetric(pm, 0, pw);
    } else if ((parentLayout = qobject_cast<QLayout *>(parent))) {
        space = parentLayout->spacing();
    }
    return qMax(space, 0);
}
