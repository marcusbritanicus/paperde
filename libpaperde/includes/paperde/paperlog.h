/*
  *
  * This file is a part of libpaperde.
  * Library for handling various services related to Paper Desktop
  * Copyright 2020 CuboCore Group
  *
  *
  *
  * This program is free software; you can redistribute it and/or modify
  * it under the terms of the GNU General Public License as published by
  * the Free Software Foundation; either version 3 of the License, or
  * (at your option) any later version.
  *
  *
  *
  * This program is distributed in the hope that it will be useful,
  * but WITHOUT ANY WARRANTY; without even the implied warranty of
  * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  * GNU General Public License for more details.
  *
  *
  *
  * You should have received a copy of the GNU General Public License
  * along with this program; if not, write to the Free Software
  * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
  * MA 02110-1301, USA.
  *
  */

#pragma once

#include <QtCore>

namespace Paper {
extern FILE *log;
extern bool SHOW_INFO_ON_CONSOLE;
extern bool SHOW_DEBUG_ON_CONSOLE;
extern bool SHOW_WARNING_ON_CONSOLE;
extern bool SHOW_CRITICAL_ON_CONSOLE;
extern bool SHOW_FATAL_ON_CONSOLE;

extern const char *COLOR_INFO;
extern const char *COLOR_DEBUG;
extern const char *COLOR_WARN;
extern const char *COLOR_CRITICAL;
extern const char *COLOR_FATAL;
extern const char *COLOR_RESET;

void Logger(QtMsgType type, const QMessageLogContext &context, const QString &message);
} // namespace Paper
