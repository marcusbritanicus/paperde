/* BEGIN_COMMON_COPYRIGHT_HEADER
 * (c)LGPL2+
 *
 * LXQt - a lightweight, Qt based, desktop toolset
 * https://lxqt.org
 *
 * Copyright: 2015 LXQt team
 * Authors:
 *  Balázs Béla <balazsbela[at]gmail.com>
 *  Paulo Lieuthier <paulolieuthier@gmail.com>
 *
 * This program or library is free software; you can redistribute it
 * and/or modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General
 * Public License along with this library; if not, write to the
 * Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
 * Boston, MA 02110-1301 USA
 *
 * END_COMMON_COPYRIGHT_HEADER */

#pragma once

#include <QGroupBox>
#include <QWidget>

namespace Paper {
class DynamicLayout;
}

class StatusNotifierButton;
class QDBusInterface;

class StatusNotifierWidget : public QGroupBox
{
    Q_OBJECT

public:
    enum LayoutStyle {
        Horizontal = 0x458651,
        Vertical,
        Grid,
    };

    StatusNotifierWidget(LayoutStyle lytStyle = Grid, QWidget *parent = nullptr);
    ~StatusNotifierWidget();

    /* Resize the buttons and the widget */
    void setButtonSize(int);

    /* Set the layout style */
    void setLayoutStyle(LayoutStyle lytStyle);

    /* Set the spacing between the buttons */
    void setItemSpacing(int);

    /* Set the margins for the widget */
    void setMargins(QMargins);

    /* Necessary but comfortable size of the widget */
    QSize minimumSizeHint() const;

public slots:
    void itemAdded(QString serviceAndPath);
    void itemRemoved(QString serviceAndPath);

private:
    QHash<QString, StatusNotifierButton *> mServices;

    Paper::DynamicLayout *sniLyt;
    LayoutStyle mLytStyle = Grid;

    int mItemSpacing = 0;
    QMargins mMargins;
    int mSize = 32;
};
