/**
  * This file is a part of PaperShell.
  * PaperShell is the Desktop Shell App for Paper Desktop
  * Copyright 2020 CuboCore Group
  *
  * This program is free software; you can redistribute it and/or modify
  * it under the terms of the GNU General Public License as published by
  * the Free Software Foundation; either version 3 of the License, or
  * (at your option) any later version.
  *
  * This program is distributed in the hope that it will be useful,
  * but WITHOUT ANY WARRANTY; without even the implied warranty of
  * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  * GNU General Public License for more details.
  *
  * You should have received a copy of the GNU General Public License
  * along with this program; if not, write to the Free Software
  * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
  * MA 02110-1301, USA.
  **/

#include <QDebug>
#include <QGraphicsDropShadowEffect>
#include <QGroupBox>
#include <QMouseEvent>
#include <QPainter>
#include <QPluginLoader>
#include <QScreen>
#include <QScroller>

#include "sni/statusnotifierwidget.h"
#include <cprime/cplugininterface.h>
#include <network/networkwidget.h>

#include "global.h"
#include <paper-config.h>

#include "UI/paperwidgets.h"
#include "ui_paperwidgets.h"

paperwidgets::paperwidgets()
    : QWidget()
    , ui(new Ui::paperwidgets)
    , uiMode(0)
    , existingWidgetsItemCount(0)
    , sniTray(nullptr)
{
    setWindowFlags(windowFlags() | Qt::BypassWindowManagerHint | Qt::FramelessWindowHint);
    setAttribute(Qt::WA_TranslucentBackground);

    ui->setupUi(this);

    loadSettings();
    prepareWidgetView();
    setupWidgets();
}

paperwidgets::~paperwidgets()
{
    delete sniTray;
    delete ui;
}

void paperwidgets::setShownAsBackground(bool yes)
{
    mBackgroundMode = yes;
    update();
}

/**
  * @brief Hook to reload the plugin when the plugin settings have changed.
  */
void paperwidgets::reloadPlugins()
{
    qDebug() << "Reload plugins called=======================";
    QStringList updatedPlugins = shellSett->value("Plugins/SelectedPlugins");

    // TODO Update only when changes occurred

    QStringList removedPlugins, newPlugins, notChangedPlugins;

    // List all the removed plugins
    Q_FOREACH (QString plugin, selectedPlugins) {
        if (not updatedPlugins.contains(plugin)) {
            removedPlugins.append(plugin);
        } else {
            notChangedPlugins.append(plugin);
        }
    }

    // List all the newly added plugins
    Q_FOREACH (QString plugin, updatedPlugins) {
        if (not selectedPlugins.contains(plugin)) {
            newPlugins.append(plugin);
        }
    }

    // Remove the plugins
    Q_FOREACH (QString plugin, removedPlugins) {
        ui->widgetLayout->removeWidget(m_pluginsWidgets[plugin]);
        delete m_pluginsWidgets[plugin];
        m_pluginsWidgets.remove(plugin);
    }

    // Insert the new plugins in exact position
    Q_FOREACH (QString plugin, newPlugins) {
        loadPlugin(plugin, updatedPlugins.indexOf(plugin) + existingWidgetsItemCount);
    }

    // Rearrange the not changed ones
    Q_FOREACH (QString plugin, notChangedPlugins) {
        ui->widgetLayout->removeWidget(m_pluginsWidgets[plugin]);
        ui->widgetLayout->insertWidget(updatedPlugins.indexOf(plugin) + existingWidgetsItemCount,
                                       m_pluginsWidgets[plugin]);
    }

    selectedPlugins = updatedPlugins;
}

/**
  * @brief Loads application settings
  */
void paperwidgets::loadSettings()
{
    uiMode = shellSett->value("Personalization/UIMode");
    selectedPlugins = shellSett->value("Plugins/SelectedPlugins");
    iconSize = shellSett->value("DockBar/IconSize");
}

void paperwidgets::prepareWidgetView()
{
    QSize scrnSize = QGuiApplication::primaryScreen()->size();

    int width = scrnSize.width();
    int height = scrnSize.height();

    maxWidth = width * 0.23;

    // set coreaction widith
    setFixedSize(maxWidth, height);

    ui->widgets->setFixedWidth(maxWidth);
    ui->widgetsL->setFixedWidth(maxWidth);

    ui->verticalLayout->setAlignment(Qt::AlignVCenter | Qt::AlignTop);
    ui->widgetLayout->setAlignment(Qt::AlignVCenter | Qt::AlignTop);
}

void paperwidgets::setupWidgets()
{
    // Detect screen rotation and take necessary steps
    QScreen *scrn = qApp->primaryScreen();

    connect(scrn, &QScreen::availableGeometryChanged, [this, scrn](const QRect &) {
        qDebug() << "Screen size Changed to: " << scrn->size();
        prepareWidgetView();
    });

    QScroller::grabGesture(ui->widgets, QScroller::TouchGesture);

    sniTray = new StatusNotifierWidget(StatusNotifierWidget::Grid);
    ui->actions->insertWidget(1, sniTray);
    sniTray->setFixedWidth(maxWidth);

    if (!selectedPlugins.count()) {
        qDebug() << "No widgets are selected";
    }

    existingWidgetsItemCount = ui->widgetLayout->count();

    QTimer::singleShot(1000, this, &paperwidgets::loadSelectedPlugins);
}

void paperwidgets::loadSelectedPlugins()
{
    int index = existingWidgetsItemCount;

    Q_FOREACH (QString pPath, selectedPlugins) {
        loadPlugin(pPath, index++);
    }

    ui->widgetLayout->addStretch();
}

bool paperwidgets::loadPlugin(const QString &pluginPath, int insertIndex)
{
    qInfo() << "Loading " << pluginPath;
    QPluginLoader loader(pluginPath);
    QObject *pObject = loader.instance();

    if (pObject) {
        WidgetsInterface *plugin = qobject_cast<WidgetsInterface *>(pObject);
        qApp->processEvents();

        if (!plugin) {
            qWarning() << "Plugin Error:" << pluginPath << loader.errorString();
            return false;
        }

        QWidget *w = plugin->widget(this);
        w->setFixedWidth(ui->widgetsL->width() - 10);
        ui->widgetLayout->insertWidget(insertIndex, w);

        qApp->processEvents();

        m_pluginsWidgets[pluginPath] = w;
    } else {
        qWarning() << "Plugin Error:" << pluginPath << loader.errorString();
        return false;
    }

    qApp->processEvents();

    return true;
}

void paperwidgets::paintEvent(QPaintEvent *pEvent)
{
    QPainter painter(this);

    painter.setPen(Qt::NoPen);

    QPalette pltt(palette());
    QColor clr(pltt.color(QPalette::Window));

    painter.setRenderHints(QPainter::Antialiasing);

    QRect bgRect;

    if (mBackgroundMode) {
        bgRect = rect().adjusted(2, 2, -2, -2);
        clr.setAlphaF(0.35);
    }

    else {
        bgRect = rect();
        clr.setAlphaF(0.9);
    }

    painter.setBrush(clr);
    painter.drawRect(bgRect);

    QWidget::paintEvent(pEvent);
}
