/*
  *
  * This file is a part of PaperShell.
  * PaperShell is the Desktop Shell App for Paper Desktop
  * Copyright 2020 CuboCore Group
  *
  *
  *
  * This program is free software; you can redistribute it and/or modify
  * it under the terms of the GNU General Public License as published by
  * the Free Software Foundation; either version 3 of the License, or
  * (at your option) any later version.
  *
  *
  *
  * This program is distributed in the hope that it will be useful,
  * but WITHOUT ANY WARRANTY; without even the implied warranty of
  * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  * GNU General Public License for more details.
  *
  *
  *
  * You should have received a copy of the GNU General Public License
  * along with this program; if not, write to the Free Software
  * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
  * MA 02110-1301, USA.
  *
  */

#include <QDebug>
#include <QtDBus>

#include "powerbackend.h"

PaperPowerBackend::PaperPowerBackend()
{
    mPowerMode = PaperPowerBackend::PowerModeNormal;

    upower = new QDBusInterface("org.freedesktop.UPower",
                                "/org/freedesktop/UPower",
                                "org.freedesktop.UPower",
                                QDBusConnection::systemBus());

    display = new QDBusInterface("org.freedesktop.UPower",
                                 "/org/freedesktop/UPower/devices/DisplayDevice",
                                 "org.freedesktop.UPower.Device",
                                 QDBusConnection::systemBus());

    mLidOpen = not upower->property("LidIsOpen").toBool();
    mOnBattery = upower->property("OnBattery").toBool();
    mLastPercentage = display->property("Percentage").toDouble();

    display->call("Refresh");
}

void PaperPowerBackend::startManagement()
{
    updateDevices();

    /* Battery/PowerSupply changes */
    QDBusConnection::systemBus().connect("org.freedesktop.UPower",
                                         "/org/freedesktop/UPower/devices/DisplayDevice",
                                         "org.freedesktop.DBus.Properties",
                                         "PropertiesChanged",
                                         this,
                                         SLOT(
                                             handlePowerChanges(QString, QVariantMap, QStringList)));

    /* For LidClosed/OnBattery changes */
    QDBusConnection::systemBus().connect("org.freedesktop.UPower",
                                         "/org/freedesktop/UPower",
                                         "org.freedesktop.DBus.Properties",
                                         "PropertiesChanged",
                                         this,
                                         SLOT(
                                             handlePowerChanges(QString, QVariantMap, QStringList)));

    /* Power Device Added */
    QDBusConnection::systemBus().connect("org.freedesktop.UPower",
                                         "/org/freedesktop/UPower",
                                         "org.freedesktop.UPower",
                                         "DeviceAdded",
                                         this,
                                         SLOT(deviceAdded(QDBusObjectPath)));

    /* Power Device Removed */
    QDBusConnection::systemBus().connect("org.freedesktop.UPower",
                                         "/org/freedesktop/UPower",
                                         "org.freedesktop.UPower",
                                         "DeviceRemoved",
                                         this,
                                         SLOT(deviceRemoved(QDBusObjectPath)));
}

QMap<QString, Device *> PaperPowerBackend::devices()
{
    return mDevices;
}

qlonglong PaperPowerBackend::getTimeToFull()
{
    display->call("Refresh");
    mETF = display->property("TimeToFull").toLongLong();
    return mETF;
}

qlonglong PaperPowerBackend::getTimeToEmpty()
{
    display->call("Refresh");
    mETR = display->property("TimeToEmpty").toLongLong();
    return mETR;
}

bool PaperPowerBackend::onBattery()
{
    display->call("Refresh");
    return mOnBattery;
}

double PaperPowerBackend::batteryCharge()
{
    display->call("Refresh");

    /* Update mLastPercentage */
    mLastPercentage = display->property("Percentage").toDouble();
    return mLastPercentage;
}

void PaperPowerBackend::updateDevices()
{
    mDevices.clear();

    QStringList result;

    QDBusInterface iface("org.freedesktop.UPower",
                         "/org/freedesktop/UPower/devices",
                         "org.freedesktop.DBus.Introspectable",
                         QDBusConnection::systemBus());
    QDBusPendingReply<QString> reply = iface.call("Introspect");

    if (reply.isError()) {
        qWarning() << "Failed to fetch devices";
        return;
    }

    QXmlStreamReader xml(reply.value());

    if (xml.hasError()) {
        qDebug() << "Failed to parse devices XML.";
        return;
    }

    QList<QDBusObjectPath> objects;

    while (!xml.atEnd()) {
        xml.readNext();
        if ((xml.tokenType() == QXmlStreamReader::StartElement)
            && (xml.name().toString() == "node")) {
            QString name = xml.attributes().value("name").toString();
            if (not name.isEmpty()) {
                QString path("/org/freedesktop/UPower/devices/" + name);
                if (mDevices.contains(path)) {
                    continue;
                }

                qDebug() << "Added device" << name;
                Device *newDevice = new Device(path, this);
                connect(newDevice, &Device::deviceChanged, this, &PaperPowerBackend::deviceChanged);
                mDevices[path] = newDevice;
            }
        }
    }
}

void PaperPowerBackend::deviceChanged(QString path)
{
    qDebug() << path << "changed";
}

void PaperPowerBackend::handlePowerChanges(QString interface, QVariantMap valueMap, QStringList)
{
    Q_UNUSED(interface);
    Q_UNUSED(valueMap);

    display->call("Refresh");

    /* Check for Lid State Changes */
    bool lidOpen = not upower->property("LidIsClosed").toBool();

    if (lidOpen != mLidOpen) {
        mLidOpen = lidOpen;
        if (mLidOpen) {
            emit lidOpened();
        }

        else {
            emit lidClosed();
        }
    }

    bool battery = upower->property("OnBattery").toBool();

    if (battery != mOnBattery) {
        mOnBattery = battery;
        if (mOnBattery) {
            emit switchedToBattery();
            mETR = display->property("TimeToEmpty").toLongLong();
        }

        else {
            emit switchedToACPower();
            mETF = display->property("TimeToFull").toLongLong();
        }
    }

    if (battery) {
        mETR = display->property("TimeToEmpty").toLongLong();

        emit timeToEmpty(mETR);

        /* If only around 1m remains, intimate the user */
        /* Take CriticalAction */
        if ((mETR <= 60) and (not mSignalled60)) {
            emit batteryEmpty();
            mSignalled60 = true;
        }

        /* If only around 5m remains, intimate the user */
        /* Take CriticalAction */
        else if ((mETR <= 300) and (not mSignalled300)) {
            emit batteryNearlyEmpty();
            mSignalled300 = true;
        }
    } else {
        mETF = display->property("TimeToFull").toLongLong();

        emit timeToFull(mETF);

        /* Reset the signalled flags when charging */
        /* This way, user is informed again when the power is lost */
        mSignalled60 = false;
        mSignalled300 = false;
    }

    /* Battery percentage */
    double curBatPC = display->property("Percentage").toDouble();

    if (mLastPercentage != curBatPC) {
        mLastPercentage = curBatPC;
        qDebug() << "Battery Charge:" << mLastPercentage;
        emit batteryChargeChanged(mLastPercentage);
        if (mLastPercentage == 100) {
            emit batteryFullyCharged();
        }
    }
}

void PaperPowerBackend::deviceAdded(QDBusObjectPath)
{
    updateDevices();
}

void PaperPowerBackend::deviceRemoved(QDBusObjectPath)
{
    updateDevices();
}
