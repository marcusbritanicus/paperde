/*
  *
  * This file is a part of PaperShell.
  * PaperShell is the Desktop Shell App for Paper Desktop
  * Copyright 2020 CuboCore Group
  *
  *
  *
  * This program is free software; you can redistribute it and/or modify
  * it under the terms of the GNU General Public License as published by
  * the Free Software Foundation; either version 3 of the License, or
  * (at your option) any later version.
  *
  *
  *
  * This program is distributed in the hope that it will be useful,
  * but WITHOUT ANY WARRANTY; without even the implied warranty of
  * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  * GNU General Public License for more details.
  *
  *
  *
  * You should have received a copy of the GNU General Public License
  * along with this program; if not, write to the Free Software
  * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
  * MA 02110-1301, USA.
  *
  */

#pragma once

#include <QDBusInterface>
#include <QWidget>

namespace Ui {
class PowerWidget;
}

namespace CPrime {
class MessageEngine;
}

class PaperPowerBackend;

class PowerWidget : public QWidget
{
    Q_OBJECT

public:
    explicit PowerWidget(QWidget *parent = nullptr);
    ~PowerWidget();

private:
    Ui::PowerWidget *ui;

    PaperPowerBackend *paperPower;
    // CPrime::MessageEngine *notifier;

    bool onBattery;

private Q_SLOTS:

    /* Lid Open/Close Signals */
    void handleLidOpened();
    void handleLidClosed();

    /* Battery Charge/Discharge Signals */
    void handleSwitchedToBattery();
    void handleSwitchedToACPower();

    /* Battery charge changed */
    void handleBatteryChargeChanged(double);

    void handleTimeToFull(qlonglong);
    void handleTimeToEmpty(qlonglong);

    /* Battery full signal */
    void handleBatteryFullyCharged();

    /* Battery nearly (5m) and almost (1m) empty */
    void handleBatteryNearlyEmpty();
    void handleBatteryEmpty();
};
