/*
  *
  * This file is a part of PaperShell.
  * PaperShell is the Desktop Shell App for Paper Desktop
  * Copyright 2020 CuboCore Group
  *
  *
  *
  * This program is free software; you can redistribute it and/or modify
  * it under the terms of the GNU General Public License as published by
  * the Free Software Foundation; either version 3 of the License, or
  * (at your option) any later version.
  *
  *
  *
  * This program is distributed in the hope that it will be useful,
  * but WITHOUT ANY WARRANTY; without even the implied warranty of
  * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  * GNU General Public License for more details.
  *
  *
  *
  * You should have received a copy of the GNU General Public License
  * along with this program; if not, write to the Free Software
  * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
  * MA 02110-1301, USA.
  *
  */

#include "powerwidget.h"
#include "ui_powerwidget.h"
#include <QDBusReply>
#include <QDebug>
#include <QProcess>
#include <QTime>

#include "powerbackend.h"
#include <cprime/messageengine.h>

PowerWidget::PowerWidget(QWidget *parent)
    : QWidget(parent)
    , ui(new Ui::PowerWidget)
{
    ui->setupUi(this);

    paperPower = new PaperPowerBackend();
    paperPower->startManagement();

    // notifier = CPrime::MessageEngine::instance();

    connect(paperPower, &PaperPowerBackend::lidOpened, this, &PowerWidget::handleLidOpened);
    connect(paperPower, &PaperPowerBackend::lidClosed, this, &PowerWidget::handleLidClosed);

    connect(paperPower,
            &PaperPowerBackend::switchedToBattery,
            this,
            &PowerWidget::handleSwitchedToBattery);
    connect(paperPower,
            &PaperPowerBackend::switchedToACPower,
            this,
            &PowerWidget::handleSwitchedToACPower);

    connect(paperPower,
            &PaperPowerBackend::batteryChargeChanged,
            this,
            &PowerWidget::handleBatteryChargeChanged);

    connect(paperPower, &PaperPowerBackend::timeToFull, this, &PowerWidget::handleTimeToFull);
    connect(paperPower, &PaperPowerBackend::timeToEmpty, this, &PowerWidget::handleTimeToEmpty);

    connect(paperPower,
            &PaperPowerBackend::batteryFullyCharged,
            this,
            &PowerWidget::handleBatteryFullyCharged);

    connect(paperPower,
            &PaperPowerBackend::batteryNearlyEmpty,
            this,
            &PowerWidget::handleBatteryNearlyEmpty);
    connect(paperPower, &PaperPowerBackend::batteryEmpty, this, &PowerWidget::handleBatteryEmpty);

    QPalette pltt(palette());

    pltt.setColor(QPalette::Base, Qt::transparent);
    setPalette(pltt);

    onBattery = paperPower->onBattery();
    if (onBattery) {
        ui->powerStatus->setIcon(QIcon::fromTheme("battery-discharging"));
    } else {
        ui->powerStatus->setIcon(QIcon::fromTheme("battery-charging"));
    }

    double batterCharge = paperPower->batteryCharge();

    handleBatteryChargeChanged(batterCharge);

    handleTimeToFull(paperPower->getTimeToFull());
    handleTimeToEmpty(paperPower->getTimeToEmpty());
}

PowerWidget::~PowerWidget()
{
    delete ui;
}

/* Lid Open/Close Signals */
void PowerWidget::handleLidOpened()
{
    // Nothing required as of now: Wayfire handles this with swaylock + swayidle
}

void PowerWidget::handleLidClosed()
{
    // Nothing required as of now: Wayfire handles this with swaylock + swayidle
}

/* Battery Charge/Discharge Signals */
void PowerWidget::handleSwitchedToBattery()
{
    // 1. Send a notification using notify-send.
    // 2. Set/Restore display brightness
    // 3. Load/Unload power profiles

    QMap<QString, Device *> devices = paperPower->devices();

    for (QString dev : devices.keys()) {
        devices[dev]->update();

        if (devices[dev]->type == Device::DeviceBattery) {
            if (devices[dev]->state == Device::DeviceStateDischarging) {
                qDebug() << "Device running on battery.";
                qDebug() << devices[dev]->name << "is discharging.";
                qDebug() << "Estimated time remaining" << devices[dev]->timeToEmpty << "seconds";
            }
        }

        else if (devices[dev]->type == Device::DeviceUps) {
            qDebug() << "Device running on UPS.";
            qDebug() << devices[dev]->name << "is discharging.";
            qDebug() << "Estimated time remaining" << devices[dev]->timeToEmpty << "seconds";
        }
    }

    onBattery = true;

    handleBatteryChargeChanged(paperPower->batteryCharge());

    // notifier->showNotification(
    //     "paper",
    //     "PaperShell",
    //     "Battery Discharging",
    //     "The AC Power supply has been disconnected. You're running on battery backup.",
    //     2000
    // );

    QProcess::startDetached(
        "notify-send",
        {"-i",
         "paper",
         "-a",
         "Paper Desktop Power Manager",
         "-t",
         "2000",
         "AC Disconnected",
         "The AC Power supply has been disconnected. You're running on battery backup."});
}

void PowerWidget::handleSwitchedToACPower()
{
    // 1. Send a notification using notify-send.
    // 2. Set/Restore display brightness
    // 3. Load/Unload power profiles

    qDebug() << "Switched to AC Power";
    onBattery = false;

    handleBatteryChargeChanged(paperPower->batteryCharge());

    // notifier->showNotification(
    //     "paper",
    //     "PaperShell",
    //     "Battery Charging",
    //     "The AC Power supply has been connected. Your battery is charging.",
    //     2000
    // );

    QProcess::startDetached("notify-send",
                            {"-i",
                             "paper",
                             "-a",
                             "Paper Desktop Power Manager",
                             "-t",
                             "2000",
                             "AC Connected",
                             "The AC Power supply has been connected. Your battery is charging."});
}

/* Battery charge changed */
void PowerWidget::handleBatteryChargeChanged(double value)
{
    // When discharging
    //  1. Set the display brightness based on percentage power left.
    //  2. oad/Unload suitable power modules.
    // When charging, change the battery icon.

    QString deviceName = qgetenv("USER");

    QString state;
    QString s = QString::number(value);

    if (onBattery) {
        state = "Battery ";
    } else {
        state = "Plugged In ";
    }

    ui->batteryLevel->setText(state + s + "%");
    ui->batteryName->setText(deviceName);

    qDebug() << "Battery charge:" << value;
    if (onBattery) {
        if (value < 5) {
            ui->powerStatus->setIcon(QIcon::fromTheme("battery-000"));
        }

        else if (value < 10) {
            ui->powerStatus->setIcon(QIcon::fromTheme("battery-005"));
        }

        else if (value < 20) {
            ui->powerStatus->setIcon(QIcon::fromTheme("battery-010"));
        }

        else if (value < 40) {
            ui->powerStatus->setIcon(QIcon::fromTheme("battery-020"));
        }

        else if (value < 40) {
            ui->powerStatus->setIcon(QIcon::fromTheme("battery-030"));
        }

        else if (value < 50) {
            ui->powerStatus->setIcon(QIcon::fromTheme("battery-040"));
        }

        else if (value < 60) {
            ui->powerStatus->setIcon(QIcon::fromTheme("battery-050"));
        }

        else if (value < 70) {
            ui->powerStatus->setIcon(QIcon::fromTheme("battery-060"));
        }

        else if (value < 80) {
            ui->powerStatus->setIcon(QIcon::fromTheme("battery-070"));
        }

        else if (value < 90) {
            ui->powerStatus->setIcon(QIcon::fromTheme("battery-080"));
        }

        else if (value < 100) {
            ui->powerStatus->setIcon(QIcon::fromTheme("battery-090"));
        }

        else {
            ui->powerStatus->setIcon(QIcon::fromTheme("battery-100"));
        }
    }

    else {
        if (value < 5) {
            ui->powerStatus->setIcon(QIcon::fromTheme("battery-000-charging"));
        }

        else if (value < 10) {
            ui->powerStatus->setIcon(QIcon::fromTheme("battery-005-charging"));
        }

        else if (value < 20) {
            ui->powerStatus->setIcon(QIcon::fromTheme("battery-010-charging"));
        }

        else if (value < 30) {
            ui->powerStatus->setIcon(QIcon::fromTheme("battery-020-charging"));
        }

        else if (value < 40) {
            ui->powerStatus->setIcon(QIcon::fromTheme("battery-030-charging"));
        }

        else if (value < 50) {
            ui->powerStatus->setIcon(QIcon::fromTheme("battery-040-charging"));
        }

        else if (value < 60) {
            ui->powerStatus->setIcon(QIcon::fromTheme("battery-050-charging"));
        }

        else if (value < 70) {
            ui->powerStatus->setIcon(QIcon::fromTheme("battery-060-charging"));
        }

        else if (value < 80) {
            ui->powerStatus->setIcon(QIcon::fromTheme("battery-070-charging"));
        }

        else if (value < 90) {
            ui->powerStatus->setIcon(QIcon::fromTheme("battery-080-charging"));
        }

        else if (value < 100) {
            ui->powerStatus->setIcon(QIcon::fromTheme("battery-090-charging"));
        }

        else {
            ui->powerStatus->setIcon(QIcon::fromTheme("battery-100-charging"));
        }
    }
}

void PowerWidget::handleTimeToFull(qlonglong seconds)
{
    qDebug() << "TimeToFull" << seconds;
    if (seconds > 0) {
        QTime time(0, 0);
        time = time.addSecs(seconds);
        ui->timeTo->setText(
            QString("%1%2 minute%3")
                .arg(time.hour() > 0
                         ? QString("%1 hour%2 ").arg(time.hour()).arg(time.hour() > 1 ? "s" : "")
                         : "")
                .arg(time.minute())
                .arg(time.minute() > 1 ? "s" : ""));
    } else {
        ui->timeTo->setText("");
    }
}

void PowerWidget::handleTimeToEmpty(qlonglong seconds)
{
    qDebug() << "TimeToEmpty" << seconds;
    if (seconds > 0) {
        QTime time(0, 0);
        time = time.addSecs(seconds);
        ui->timeTo->setText(
            QString("%1%2 minute%3")
                .arg(time.hour() > 0
                         ? QString("%1 hour%2 ").arg(time.hour()).arg(time.hour() > 1 ? "s" : "")
                         : "")
                .arg(time.minute())
                .arg(time.minute() > 1 ? "s" : ""));
    } else {
        ui->timeTo->setText("");
    }
}

/* Battery full signal */
void PowerWidget::handleBatteryFullyCharged()
{
    // Display a notification for the user

    qDebug() << "Charging complete";

    ui->powerStatus->setIcon(QIcon::fromTheme("battery-100"));

    // notifier->showNotification(
    //     "paper",
    //     "PaperShell",
    //     "Battery Charged",
    //     "Your battery/UPS is completely charged.",
    //     2000
    // );

    QProcess::startDetached("notify-send",
                            {"-i",
                             "paper",
                             "-a",
                             "Paper Desktop Power Manager",
                             "-t",
                             "2000",
                             "Battery Charged",
                             "Your battery/UPS is completely charged."});
}

/* Battery nearly (5m) and almost (1m) empty */
void PowerWidget::handleBatteryNearlyEmpty()
{
    // Inform the user that around 5 minutes of battery charge remain.

    // notifier->showNotification(
    //     "paper",
    //     "PaperShell",
    //     "Battery Nearly Empty",
    //     "Your battery is nearly empty with approximately 5 minutes of charge is remaining. "
    //     "Save your work immediately and find an alternate source of power supply.",
    //     -1
    // );

    ui->powerStatus->setIcon(QIcon(":/icons/battery-005.png"));
    QProcess::startDetached(
        "notify-send",
        {"-i",
         "paper",
         "-a",
         "Paper Desktop Power Manager",
         "-t",
         "-1",
         "Battery Nearly Empty",
         "Your battery is nearly empty with approximately 5 minutes of charge is remaining. "
         "Save your work immediately and find an alternate source of power supply."});
}

void PowerWidget::handleBatteryEmpty()
{
    // Inform the user that around 1 minute of battery charge remains.
    // Prepare for suspend/shutdown

    // notifier->showNotification(
    //     "paper",
    //     "PaperShell",
    //     "Battery Critical",
    //     "Your battery is has under 1 minute of charge is remaining. "
    //     "Your system will be suspended immediately unless a power supply is connected.",
    //     -1
    // );

    ui->powerStatus->setIcon(QIcon(":/icons/battery-000.png"));
    QProcess::startDetached(
        "notify-send",
        {"-i",
         "paper",
         "-a",
         "Paper Desktop Power Manager",
         "-t",
         "-1",
         "Battery Critical",
         "Your battery is has under 1 minute of charge is remaining. "
         "Your system will be suspended immediately unless a power supply is connected."});
}
