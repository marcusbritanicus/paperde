/**
  * This file is a part of PaperShell.
  * PaperShell is the Desktop Shell App for Paper Desktop
  * Copyright 2020 CuboCore Group
  *
  * This program is free software; you can redistribute it and/or modify
  * it under the terms of the GNU General Public License as published by
  * the Free Software Foundation; either version 3 of the License, or
  * (at your option) any later version.
  *
  * This program is distributed in the hope that it will be useful,
  * but WITHOUT ANY WARRANTY; without even the implied warranty of
  * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  * GNU General Public License for more details.
  *
  * You should have received a copy of the GNU General Public License
  * along with this program; if not, write to the Free Software
  * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
  * MA 02110-1301, USA.
  **/

#pragma once

#include <QtCore>

#include <wayqt/LayerShell.hpp>
#include <wayqt/Registry.hpp>
#include <wayqt/WayQtUtils.hpp>

#include "UI/paperwidgets.h"

namespace Paper {
namespace Widgets {
class Manager;
}
} // namespace Paper

class QVariantAnimation;

/**
  * @class Paper::Widgets::Manager
  * This class will handle the creation, showing, hiding and destruction
  * of paperwidgets UI on one or all screens.
  * Additionally, it will also handle the CLI requests
  */
class Paper::Widgets::Manager : public QObject
{
    Q_OBJECT;

public:
    Manager();
    ~Manager();

    /**
	  * Load the user settings.
	  */
    void loadSettings();

    /**
	  * Handle the requests from other instance. To reply to queries,
	  * write suitable data to the file descriptor @fd.
	  */
    Q_SLOT void handleMessages(QString msg, int fd);

    /**
       * Start an instance of paperwidgets * on screen @scrn
       */
    void createInstance(QString opName);

    /**
       * Stop an instance of paperwidgets * on screen named @opName
       */
    void destroyInstance(QString opName);

private:
    // Impl details: Perform the actual showing
    void showLayerSurface(QString opName);

    // Impl details: Perform the actual hiding
    void hideLayerSurface(QString opName);

    // List of screens
    QHash<QString, QScreen *> mScreens;

    // List of instances according to the screen
    QHash<QString, paperwidgets *> mInstances;

    // Note the screens on which an instances exists
    QHash<QString, bool> mInstanceStates;

    // Note the screens on which an instances exists
    QHash<QString, bool> mInstanceFDs;

    QHash<QString, WQt::LayerSurface *> mSurfaces;

    /**
	  * Dock size - we obtain this from the settings
	  * 40 is the default. Load the correct value in @loadSettings().
	  */
    int mDockSize = 40;

    /**
       * Return an animator which will animate a layer surface when started.
       * @surf - Layer surface to be moved
       * @ui   - Instance of paperwidgets (used to calculate the margin positions)
       * @show - If true, the widget is being shown, otherwise hidden
       */
    QVariantAnimation *createAnimator(QString opName, bool show);
};
