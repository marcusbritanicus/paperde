/**
  * This file is a part of PaperShell.
  * PaperShell is the Desktop Shell App for Paper Desktop
  * Copyright 2020 CuboCore Group
  *
  * This file was originally a part of DesQ project (https://gitlab.com/desq/)
  * Suitable modifications have been done to meet the needs of PaperDesktop.
  *
  * This program is free software; you can redistribute it and/or modify
  * it under the terms of the GNU General Public License as published by
  * the Free Software Foundation; either version 3 of the License, or
  * (at your option) any later version.
  *
  * This program is distributed in the hope that it will be useful,
  * but WITHOUT ANY WARRANTY; without even the implied warranty of
  * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  * GNU General Public License for more details.
  *
  * You should have received a copy of the GNU General Public License
  * along with this program; if not, write to the Free Software
  * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
  * MA 02110-1301, USA.
  **/

// Local Headers
#include "global.h"
#include "paperbg.h"

#include <paperde/paperlog.h>

#include <cprime/systemxdg.h>

// Wayland Headers
#include <wayqt/LayerShell.hpp>
#include <wayqt/Registry.hpp>
#include <wayqt/WayQtUtils.hpp>

#include <DFApplication.hpp>

Paper::Settings *shellSett;
WQt::Registry *wlRegistry;

PaperBG *setupBackground(QScreen *screen, QString bgImage)
{
    PaperBG *shell = new PaperBG(screen, bgImage);

    shell->show();

    if (WQt::Utils::isWayland()) {
        /** wl_output corresponding to @screen */
        wl_output *output = WQt::Utils::wlOutputFromQScreen(screen);

        WQt::LayerSurface *cls = wlRegistry->layerShell()->getLayerSurface(
            shell->windowHandle(),       // Window Handle
            output,                      // wl_output object - for multi-monitor support
            WQt::LayerShell::Background, // Background layer
            "bg"                         // Dummy namespace
        );

        /** Anchor everywhere */
        cls->setAnchors(WQt::LayerSurface::Top | WQt::LayerSurface::Bottom | WQt::LayerSurface::Left
                        | WQt::LayerSurface::Right);

        /** Size - when */
        cls->setSurfaceSize(shell->size());

        /** Nothing should move this */
        cls->setExclusiveZone(-1);

        /** No keyboard interaction */
        cls->setKeyboardInteractivity(WQt::LayerSurface::NoFocus);

        /** Apply the changes */
        cls->apply();

        QObject::connect(screen, &QScreen::geometryChanged, [=]() {
            shell->resizeDesktop();
            cls->setSurfaceSize(shell->size());
            cls->apply();
        });
    }

    return shell;
}

int main(int argc, char **argv)
{
    qputenv("QT_WAYLAND_USE_BYPASSWINDOWMANAGERHINT", QByteArrayLiteral("1"));

    QDir cache(CPrime::SystemXdg::userDir(CPrime::SystemXdg::XDG_CACHE_HOME));

    Paper::log = fopen(cache.filePath("paperde/PaperBG.log").toLocal8Bit().data(), "a");

    qInstallMessageHandler(Paper::Logger);

    qDebug() << "------------------------------------------------------------------------";
    qDebug() << "Paper BG started at"
             << QDateTime::currentDateTime().toString("yyyyMMddThhmmss").toUtf8().constData();
    qDebug() << "------------------------------------------------------------------------";

    DFL::Application app(argc, argv);

    app.setApplicationName("paperbg");
    app.setOrganizationName("CuboCore");
    app.setApplicationVersion("v" PROJECT_VERSION);

    QCommandLineParser parser;

    parser.addHelpOption();    // Help
    parser.addVersionOption(); // Version

    /* Path to the background file */
    parser.addOption({{"b", "background"}, "The path to the background file.", "image file"});

    /* Save this? */
    parser.addOption({{"s", "save"}, "Save the image file in settings for the next start."});

    /* Process the CLI args */
    parser.process(app);

    if (app.lockApplication()) {
        /* Init DFL::Settings object */
        shellSett = new Paper::Settings("papershell");

        wlRegistry = new WQt::Registry(WQt::Wayland::display());

        wlRegistry->setup();

        qDebug() << "Starting paperbg";
        QString bgValue(parser.value("background"));

        for (QScreen *screen : qApp->screens()) {
            PaperBG *shell = setupBackground(screen, bgValue);
            QObject::connect(&app,
                             &DFL::Application::messageFromClient,
                             shell,
                             &PaperBG::handleMessages);

            QObject::connect(&app, &DFL::Application::screenRemoved, [=](QScreen *scrn) {
                if (scrn == screen) {
                    shell->close();
                    delete shell;
                }
            });
        }

        /** Dynamically start/stop the background layer surface for each added/removed screen */
        QObject::connect(&app, &QApplication::screenAdded, [&app, bgValue](QScreen *screen) {
            PaperBG *shell = setupBackground(screen, bgValue);
            QObject::connect(&app,
                             &DFL::Application::messageFromClient,
                             shell,
                             &PaperBG::handleMessages);
            QObject::connect(&app, &DFL::Application::screenRemoved, [=](QScreen *scrn) {
                if (scrn == screen) {
                    shell->close();
                    delete shell;
                }
            });
        });

        return app.exec();
    }

    else {
        if (parser.isSet("background")) {
            QString msg;

            if (parser.isSet("save")) {
                msg = "set-";
            }

            msg += "background " + parser.value("background");
            app.messageServer(msg);
        }

        if (parser.isSet("save") and not parser.isSet("background")) {
            qDebug() << "--save cannot be used alone.";
            parser.showHelp();
        }

        return 0;
    }

    return 0;
}
