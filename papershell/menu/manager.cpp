/**
  * This file is a part of PaperShell.
  * PaperShell is the Desktop Shell App for Paper Desktop
  * Copyright 2020 CuboCore Group
  *
  * This program is free software; you can redistribute it and/or modify
  * it under the terms of the GNU General Public License as published by
  * the Free Software Foundation; either version 3 of the License, or
  * (at your option) any later version.
  *
  * This program is distributed in the hope that it will be useful,
  * but WITHOUT ANY WARRANTY; without even the implied warranty of
  * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  * GNU General Public License for more details.
  *
  * You should have received a copy of the GNU General Public License
  * along with this program; if not, write to the Free Software
  * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
  * MA 02110-1301, USA.
  **/

#include "manager.h"
#include "global.h"
#include "papermenu.h"

#include <QEasingCurve>
#include <QScreen>
#include <QSequentialAnimationGroup>
#include <QVariantAnimation>

#include <DFApplication.hpp>

Paper::Menu::Manager::Manager()
{
    for (QScreen *scrn : qApp->screens()) {
        mScreens[scrn->name()] = scrn;
        createInstance(scrn->name());
        connect(mInstances[scrn->name()], &papermenu::hideMenu, [scrn, this]() {
            hideLayerSurface(scrn->name());
            if (mInstanceFDs.contains(scrn->name())) {
                qApp->messageClient("hidden", mInstanceFDs[scrn->name()]);
            }
        });
    }

    connect(qApp, &QApplication::screenAdded, [=](QScreen *screen) {
        mScreens[screen->name()] = screen;
        createInstance(screen->name());
    });

    connect(qApp, &QApplication::screenRemoved, [=](QScreen *screen) {
        QString opName = screen->name();
        mScreens.remove(opName);

        if (mInstanceStates.contains(opName) && mInstanceStates[opName] == true) {
            destroyInstance(opName);
        }

        if (mInstances.contains(opName)) {
            papermenu *w = mInstances.take(opName);
            w->close();
            delete w;
        }
    });
}

Paper::Menu::Manager::~Manager() {}

void Paper::Menu::Manager::handleMessages(QString msg, int fd)
{
    if (msg.startsWith("toggle")) {
        QStringList parts = msg.split("\n");
        mInstanceFDs[parts[1]] = fd;
        if (mInstanceStates[parts[1]]) {
            hideLayerSurface(parts[1]);
            qApp->messageClient("hidden", fd);
        }

        else {
            showLayerSurface(parts[1]);
            qApp->messageClient("shown", fd);
        }
    }

    else {
        qWarning() << "Unhandled request:" << msg;
        qApp->messageClient("Unhandled request", fd);
    }
}

void Paper::Menu::Manager::showLayerSurface(QString opName)
{
    QVariantAnimation *showAnim = createAnimator(opName, true);

    showAnim->start();
}

void Paper::Menu::Manager::hideLayerSurface(QString opName)
{
    QVariantAnimation *hideAnim = createAnimator(opName, false);

    hideAnim->start();
}

QVariantAnimation *Paper::Menu::Manager::createAnimator(QString opName, bool show)
{
    QVariantAnimation *anim = new QVariantAnimation();

    anim->setDuration(500);

    WQt::LayerSurface *surf = mSurfaces[opName];
    papermenu *ui = mInstances[opName];

    if (show) {
        anim->setStartValue(-ui->width());
        anim->setEndValue(mDockSize);
        anim->setEasingCurve(QEasingCurve(QEasingCurve::OutCubic));
    }

    else {
        anim->setStartValue(mDockSize);
        anim->setEndValue(-ui->width());
        anim->setEasingCurve(QEasingCurve(QEasingCurve::InOutQuart));
    }

    connect(anim, &QVariantAnimation::valueChanged, [=](QVariant val) {
        surf->setMargins(QMargins(val.toInt(), 0, 0, 0));
        surf->apply();

        qDebug() << val.toInt();

        qApp->processEvents();
    });

    connect(anim, &QVariantAnimation::finished, [=]() {
        if (show) {
            surf->setKeyboardInteractivity(WQt::LayerSurface::Exclusive);
            mInstanceStates[opName] = true;
            qApp->messageClient("shown", mInstanceFDs[opName]);
        }

        else {
            surf->setKeyboardInteractivity(WQt::LayerSurface::NoFocus);
            mInstanceStates[opName] = false;
            qApp->messageClient("hidden", mInstanceFDs[opName]);
        }
    });

    return anim;
}

void Paper::Menu::Manager::createInstance(QString opName)
{
    QScreen *screen = mScreens[opName];

    mInstances[opName] = new papermenu();
    mInstances[opName]->setFixedSize(screen->size() - QSize(mDockSize, 0));

    if (WQt::Utils::isWayland()) {
        mInstances[opName]->show();

        /** wl_output corresponding to @screen */
        wl_output *output = WQt::Utils::wlOutputFromQScreen(screen);

        WQt::LayerSurface *cls = wlRegistry->layerShell()->getLayerSurface(
            mInstances[opName]->windowHandle(), // Window Handle
            output,                             // wl_output object - for multi-monitor support
            WQt::LayerShell::Top,               // Top Layer
            "papermenu"                         // Dummy namespace
        );

        /** Anchor everywhere */
        cls->setAnchors(WQt::LayerSurface::Top | WQt::LayerSurface::Bottom
                        | WQt::LayerSurface::Left);

        /** Size - when */
        cls->setSurfaceSize(mInstances[opName]->size());

        /** Nothing should move this */
        cls->setExclusiveZone(-1);

        /** No keyboard interaction */
        cls->setKeyboardInteractivity(WQt::LayerSurface::NoFocus);

        /** Default margins: hide the surface */
        cls->setMargins(QMargins(-mInstances[opName]->width(), 0, 0, 0));

        /** Apply the changes */
        cls->apply();

        mSurfaces[opName] = cls;
        mInstanceStates[opName] = false;
    }
}

void Paper::Menu::Manager::destroyInstance(QString opName)
{
    if (mSurfaces.contains(opName)) {
        WQt::LayerSurface *surf = mSurfaces.take(opName);
        delete surf;
    }

    if (mInstanceStates.contains(opName)) {
        mInstanceStates.remove(opName);
    }
}
