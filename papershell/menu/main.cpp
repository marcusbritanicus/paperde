/**
  * This file is a part of PaperShell.
  * PaperShell is the Desktop Shell App for Paper Desktop
  * Copyright 2020 CuboCore Group
  *
  * This program is free software; you can redistribute it and/or modify
  * it under the terms of the GNU General Public License as published by
  * the Free Software Foundation; either version 3 of the License, or
  * (at your option) any later version.
  *
  * This program is distributed in the hope that it will be useful,
  * but WITHOUT ANY WARRANTY; without even the implied warranty of
  * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  * GNU General Public License for more details.
  *
  * You should have received a copy of the GNU General Public License
  * along with this program; if not, write to the Free Software
  * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
  * MA 02110-1301, USA.
  **/

#include <QDebug>
#include <QMessageBox>
#include <QScreen>
#include <QTime>

#include <cprime/capplication.h>
#include <cprime/systemxdg.h>
#include <paperde/paperlog.h>

#include <DFApplication.hpp>

#include <wayqt/LayerShell.hpp>
#include <wayqt/Registry.hpp>
#include <wayqt/WayQtUtils.hpp>

#include "global.h"
#include "manager.h"

#include <paper-config.h>

Paper::Settings *shellSett = nullptr;
WQt::Registry *wlRegistry = nullptr;

int main(int argc, char *argv[])
{
    /* Disable window manager hints for this window */
    qputenv("QT_WAYLAND_USE_BYPASSWINDOWMANAGERHINT", QByteArrayLiteral("1"));

    QDir cache(CPrime::SystemXdg::userDir(CPrime::SystemXdg::XDG_CACHE_HOME));

    Paper::log = fopen(cache.filePath("paperde/PaperMenu.log").toLocal8Bit().data(), "a");

    qInstallMessageHandler(Paper::Logger);

    qDebug() << "------------------------------------------------------------------------";
    qDebug() << "Paper Menu started at"
             << QDateTime::currentDateTime().toString("yyyyMMddThhmmss").toUtf8().constData();
    qDebug() << "------------------------------------------------------------------------";

    DFL::Application app(argc, argv);

    // Set application info
    app.setOrganizationName("CuboCore");
    app.setApplicationName(APPNAME);
    app.setApplicationVersion(QStringLiteral(PAPER_VERSION_STR));

    QCommandLineParser parser;

    parser.addHelpOption();    // Help
    parser.addVersionOption(); // Version

    parser.addOption({{"t", "toggle"}, "Show/hide the PaperMenu isntance"});
    parser.addOption(
        {{"o", "output"}, "Name of the output on which menu is to be shown.", "output"});

    parser.process(app);

    if (app.lockApplication()) {
        shellSett = new Paper::Settings("papershell");
        wlRegistry = new WQt::Registry(WQt::Wayland::display());
        wlRegistry->setup();

        Paper::Menu::Manager *pmMgr = new Paper::Menu::Manager();

        QObject::connect(&app,
                         &DFL::Application::messageFromClient,
                         pmMgr,
                         &Paper::Menu::Manager::handleMessages);

        return app.exec();
    }

    else {
        if (parser.isSet("toggle")) {
            if (not parser.isSet("output")) {
                qCritical()
                    << "Please specify the output on which PaperDock instance is to be toggled.";
                return 1;
            }

            app.messageServer("toggle\n" + parser.value("output"));
        }

        else {
            printf("Paper Dock v" PAPER_VERSION_STR "\n");
            parser.showHelp();
        }

        return 0;
    }

    return 0;
}
