# PaperShell
The Desktop Shell component of the Paper Desktop.

### Download
You can download latest release of Paper Desktop from the links below
* [Source](https://gitlab.com/cubocore/paper/paperdesktop/tags)
* [ArchPackages](https://gitlab.com/cubocore/wiki/tree/master/ArchPackages)

### Dependencies:
* qt5-base
* qt5-wayland
* wayland
* wayland-protocols
* libxcomposite
* libxkbcommon
* [libcprime](https://gitlab.com/cubocore/libcprime.git)
* [wayqt](https://gitlab.com/desktop-frameworks/wayqt.git)
* [wayfire](https://github.com/WayfireWM/wayfire/) - runtime

### Information
Please see the Wiki page for this info.[Wiki page](https://gitlab.com/cubocore/wiki) .
* Changes in latest release
* Build from the source
* Tested In
* Known Bugs
* Help Us

### Feedback
* We need your feedback to improve the Paper Desktop. Send us your feedback through GitLab [issues](https://gitlab.com/cubocore/paper/paperdesktop/-/issues).
