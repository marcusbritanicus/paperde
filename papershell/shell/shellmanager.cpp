/**
  * This file is a part of PaperShell.
  * PaperShell is the Desktop Shell App for Paper Desktop
  * Copyright 2020 CuboCore Group
  *
  * This file was originally a part of DesQ project (https://gitlab.com/desq/)
  * Suitable modifications have been done to meet the needs of PaperDesktop.
  *
  * This program is free software; you can redistribute it and/or modify
  * it under the terms of the GNU General Public License as published by
  * the Free Software Foundation; either version 3 of the License, or
  * (at your option) any later version.
  *
  * This program is distributed in the hope that it will be useful,
  * but WITHOUT ANY WARRANTY; without even the implied warranty of
  * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  * GNU General Public License for more details.
  *
  * You should have received a copy of the GNU General Public License
  * along with this program; if not, write to the Free Software
  * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
  * MA 02110-1301, USA.
  **/

#include <ctime>
#include <QDebug>

#include "autostart.hpp"
#include "global.h"
#include "shellmanager.hpp"
#include "trackedprocess.hpp"

#include <DFIpcClient.hpp>
#include <paper-config.h>

#include <DFApplication.hpp>

#include <wayqt/Registry.hpp>
#include <wayqt/WayQtUtils.hpp>
#include <wayqt/WayfireShell.hpp>

int MaxAutoRestartLimit;

Paper::Shell::Manager::Manager()
    : QObject()
{
    MaxAutoRestartLimit = shellSett->value("Manager/MaxAutoRestartLimit");
}

Paper::Shell::Manager::~Manager()
{
    shutdown();
}

void Paper::Shell::Manager::startManagement()
{
    QStringList utils = {
        // "paperpowermanager",	// Power manager
        "papersnwatcher", // Status Notifier Watcher
        "paperbg",        // Paper Background
        "paperdock",      // Paper Dock
        "paperwidgets",   // Paper Widgets
        "papermenu",      // Paper Menu
                          // "paperlogout"        // Paper Logout Dialog
    };

    for (QString util : utils) {
        qDebug() << "Starting" << util;

        startProcess(util, {});
    }

    PaperAutoStart *autoStart = new PaperAutoStart();
    autoStart->start();
}

void Paper::Shell::Manager::listProcesses(int fd)
{
    printf("Paper Shell v" PROJECT_VERSION "\n\n");
    printf("Currently, the following processes are running:\n");
    qApp->messageClient(activatedUtils.keys().join("\n"), fd);
}

void Paper::Shell::Manager::startProcess(QString util, QStringList args)
{
    /** Do nothing if it's already running */
    if (activatedUtils.contains(util)) {
        return;
    }

    QString utilExec = (util.startsWith("/") ? util : UtilsPath + util);
    QFileInfo utilInfo(utilExec);

    if (utilInfo.isExecutable()) {
        TrackedProcess *tp = new TrackedProcess(utilExec, args);
        tp->start();

        connect(tp, &TrackedProcess::crashed, [=]() mutable {
            if (activatedUtils.contains(util)) {
                activatedUtils.take(util)->deleteLater();
            }
        });

        activatedUtils[util] = tp;
    }
}

void Paper::Shell::Manager::killProcess(QString util)
{
    /** Do nothing if it's not running */
    if (not activatedUtils.contains(util)) {
        return;
    }

    activatedUtils.take(util)->terminate();
}

void Paper::Shell::Manager::restartProcess(QString util)
{
    /** Do nothing if it's not running */
    if (not activatedUtils.contains(util)) {
        return;
    }

    /** Restart otherwise */
    else {
        activatedUtils.value(util)->restart();
    }
}

void Paper::Shell::Manager::shutdown()
{
    delete shellSett;

    for (QString util : activatedUtils.keys()) {
        TrackedProcess *tp = activatedUtils.take(util);

        if (tp->isRunning()) {
            tp->terminate();
            delete tp;
        }
    }

    /* Close this daemon */
    qApp->quit();
}

void Paper::Shell::Manager::handleMessages(QString message, int fd)
{
    if (message == "list-processes") {
        listProcesses(fd);
    }

    else if (message.startsWith("start-process")) {
        QStringList parts = message.split("\n\n", Qt::SkipEmptyParts);
        parts.removeAt(0);

        startProcess(parts[0], (parts.count() == 2 ? parts.at(1).split("\n") : QStringList()));
    }

    else if (message.startsWith("restart-process")) {
        QStringList parts = message.split("\n", Qt::SkipEmptyParts);
        parts.removeAt(0);

        restartProcess(parts[0]);
    }

    else if (message.startsWith("kill-process")) {
        QStringList parts = message.split("\n", Qt::SkipEmptyParts);
        parts.removeAt(0);

        killProcess(parts[0]);
    }
}
