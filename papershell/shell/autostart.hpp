/**
  * This file is a part of PaperShell.
  * PaperShell is the Desktop Shell App for Paper Desktop
  * Copyright 2020 CuboCore Group
  *
  * This file was originally a part of DesQ project (https://gitlab.com/desq/)
  * Suitable modifications have been done to meet the needs of PaperDesktop.
  *
  * This program is free software; you can redistribute it and/or modify
  * it under the terms of the GNU General Public License as published by
  * the Free Software Foundation; either version 3 of the License, or
  * (at your option) any later version.
  *
  * This program is distributed in the hope that it will be useful,
  * but WITHOUT ANY WARRANTY; without even the implied warranty of
  * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  * GNU General Public License for more details.
  *
  * You should have received a copy of the GNU General Public License
  * along with this program; if not, write to the Free Software
  * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
  * MA 02110-1301, USA.
  **/

#pragma once

#include <QObject>
#include <QProcess>
#include <QSettings>
#include <QString>
#include <QThread>

class PaperAutoStart : public QThread
{
    Q_OBJECT

public:
    /* Init */
    PaperAutoStart();

public Q_SLOTS:
    /* Start the session by performing various tasks */
    void start();

protected:
    void run();

private:
    /* Start the Paper Apps that are expected to run all the time */
    void startPaperApps();

    /* Start desktop: @desktop - full path to .desktop file */
    void startDesktop(QString desktop);

    /* Start scripts: @script - full path to the script/exec to be run */
    void startScript(QString script);

    /* Restore the UI session */
    void restoreSession(QString name = "Previous");

    /* Session name */
    QString mSessionName;

    /* QSettings instance */
    QSettings *session;
};
