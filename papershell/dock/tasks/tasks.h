/**
  * This file is a part of PaperShell.
  * PaperShell is the Desktop Shell App for Paper Desktop
  * Copyright 2020 CuboCore Group
  *
  * This program is free software; you can redistribute it and/or modify
  * it under the terms of the GNU General Public License as published by
  * the Free Software Foundation; either version 3 of the License, or
  * (at your option) any later version.
  *
  * This program is distributed in the hope that it will be useful,
  * but WITHOUT ANY WARRANTY; without even the implied warranty of
  * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  * GNU General Public License for more details.
  *
  * You should have received a copy of the GNU General Public License
  * along with this program; if not, write to the Free Software
  * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
  * MA 02110-1301, USA.
  **/

#pragma once

#include <QtWidgets>

namespace WQt {
class ForeignToplevelManager;
class ForeignToplevelHandle;
} // namespace WQt

namespace Paper {
namespace Dock {
class TaskBar;
}
} // namespace Paper

class Paper::Dock::TaskBar : public QWidget
{
    Q_OBJECT;

public:
    TaskBar(QWidget *parent);

private:
    WQt::ForeignToplevelManager *winMgr;

    QVBoxLayout *tasksLyt;

    /** Mapping of WQt::ForeignToplevelHandle to QToolButton */
    QHash<WQt::ForeignToplevelHandle *, QToolButton *> mTlTaskHash;

    void addWindow(WQt::ForeignToplevelHandle *);
    void removeWindow(WQt::ForeignToplevelHandle *);

    void updateIcon(WQt::ForeignToplevelHandle *);
    void updateState();
};
