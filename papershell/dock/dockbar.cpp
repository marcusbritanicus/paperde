/**
  * This file is a part of PaperShell.
  * PaperShell is the Desktop Shell App for Paper Desktop
  * Copyright 2020 CuboCore Group
  *
  * This program is free software; you can redistribute it and/or modify
  * it under the terms of the GNU General Public License as published by
  * the Free Software Foundation; either version 3 of the License, or
  * (at your option) any later version.
  *
  * This program is distributed in the hope that it will be useful,
  * but WITHOUT ANY WARRANTY; without even the implied warranty of
  * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  * GNU General Public License for more details.
  *
  * You should have received a copy of the GNU General Public License
  * along with this program; if not, write to the Free Software
  * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
  * MA 02110-1301, USA.
  **/

#include "dockbar.h"
#include "tasks.h"

#include <QDateTime>
#include <QTimer>
#include <QtWidgets>

const static QString taskCSS = QString("QToolButton {"
                                       "   border: none;"
                                       "   border-radius: 0px;"
                                       "   background-color: transparent;"
                                       "   border-left: 2px solid %1;"
                                       "}"
                                       "QToolButton:pressed {"
                                       "   margin-left: 1px;"
                                       "   margin-top: 1px;"
                                       "   border-left: 2px solid %2;"
                                       "}"
                                       "QToolButton:hover {"
                                       "   background-color: transparent;"
                                       "   border-left: 2px solid palette(Highlight);"
                                       "}");

Paper::Dock::UI::UI(QString screenName)
    : QWidget()
{
    mOutputName = screenName;
    setWindowFlags(windowFlags() | Qt::BypassWindowManagerHint | Qt::FramelessWindowHint);
    setAttribute(Qt::WA_TranslucentBackground);

    prepareDock();
    buildUI();
}

Paper::Dock::UI::~UI() {}

void Paper::Dock::UI::setShownAsBackground(bool yes)
{
    mBackgroundMode = yes;
    update();
}

void Paper::Dock::UI::makeOpaque(bool yes)
{
    mOpaqueMode = yes;
    update();
}

void Paper::Dock::UI::markWidgets(bool yes)
{
    if (yes) {
        widgetsBtn->setStyleSheet(taskCSS.arg("palette(Highlight)", "palette(Highlight)"));
    } else {
        widgetsBtn->setStyleSheet(taskCSS.arg("transparent", "palette(Highlight)"));
    }

    update();
}

void Paper::Dock::UI::markMenu(bool yes)
{
    if (yes) {
        menuBtn->setStyleSheet(taskCSS.arg("palette(Highlight)", "palette(Highlight)"));
    } else {
        menuBtn->setStyleSheet(taskCSS.arg("transparent", "palette(Highlight)"));
    }

    update();
}

void Paper::Dock::UI::prepareDock()
{
    QSize scrnSize = QGuiApplication::primaryScreen()->size();

    int height = scrnSize.height();

    int maxWidth = 40;

    setFixedSize(maxWidth, height);
}

void Paper::Dock::UI::timerEvent(QTimerEvent *event)
{
    if (timer.timerId() == event->timerId()) {
        updateTime();
    }
}

void Paper::Dock::UI::updateTime()
{
    QDateTime dt = QDateTime::currentDateTime();

    widgetsBtn->setText(dt.toString("HH\nmm\n📶\n🔊\n🌩"));
}

void Paper::Dock::UI::buildUI()
{
    menuBtn = new QToolButton();
    menuBtn->setIcon(QIcon(":/paper.png"));
    menuBtn->setFixedSize(width(), width());
    menuBtn->setIconSize(QSize(width() - 4, width() - 4));
    menuBtn->setAutoRaise(true);
    menuBtn->setStyleSheet(taskCSS.arg("transparent", "palette(Highlight)"));

    connect(menuBtn, &QToolButton::clicked, [=]() { emit toggleMenu(mOutputName); });

    tasks = new TaskBar(this);
    tasks->setFixedWidth(width());
    tasks->setMinimumHeight(width());

    widgetsBtn = new QToolButton();
    widgetsBtn->setFixedSize(width(), 3 * width());
    widgetsBtn->setAutoRaise(true);
    widgetsBtn->setStyleSheet(taskCSS.arg("transparent", "palette(Highlight)"));

    QFont btnFont(font());
    btnFont.setWeight(QFont::Bold);
    btnFont.setPointSize(10);
    widgetsBtn->setFont(btnFont);

    connect(widgetsBtn, &QToolButton::clicked, [=]() { emit toggleWidgets(mOutputName); });

    updateTime();
    timer.start(1000, Qt::PreciseTimer, this);

    QVBoxLayout *lyt = new QVBoxLayout();
    lyt->setContentsMargins(QMargins());
    lyt->setSpacing(0);

    lyt->addWidget(menuBtn);
    lyt->addStretch();
    lyt->addWidget(tasks);
    lyt->addStretch();
    lyt->addWidget(widgetsBtn);

    setLayout(lyt);
}

void Paper::Dock::UI::paintEvent(QPaintEvent *pEvent)
{
    QPainter painter(this);

    painter.setPen(Qt::NoPen);

    QPalette pltt(palette());
    QColor clr1(pltt.color(QPalette::Window));

    painter.setRenderHints(QPainter::Antialiasing);

    QRect bgRect;

    if (mBackgroundMode) {
        bgRect = rect().adjusted(2, 2, -2, -2);
        clr1.setAlphaF(0.35);
    }

    else if (mOpaqueMode) {
        bgRect = rect();
        clr1.setAlphaF(0.90);
    }

    else {
        bgRect = rect();
        clr1.setAlphaF(0.00);
    }

    painter.setBrush(clr1);
    painter.drawRect(bgRect);
    painter.end();

    QWidget::paintEvent(pEvent);
}
