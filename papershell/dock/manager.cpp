/**
  * This file is a part of PaperShell.
  * PaperShell is the Desktop Shell App for Paper Desktop
  * Copyright 2020 CuboCore Group
  *
  * This program is free software; you can redistribute it and/or modify
  * it under the terms of the GNU General Public License as published by
  * the Free Software Foundation; either version 3 of the License, or
  * (at your option) any later version.
  *
  * This program is distributed in the hope that it will be useful,
  * but WITHOUT ANY WARRANTY; without even the implied warranty of
  * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  * GNU General Public License for more details.
  *
  * You should have received a copy of the GNU General Public License
  * along with this program; if not, write to the Free Software
  * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
  * MA 02110-1301, USA.
  **/

#include "manager.h"
#include "dockbar.h"
#include "global.h"

#include <QEasingCurve>
#include <QScreen>
#include <QSequentialAnimationGroup>
#include <QVariantAnimation>

#include <DFApplication.hpp>

Paper::Dock::Manager::Manager()
    : menuVisible(false)
    , widgetsVisible(false)
{
    for (QScreen *scrn : qApp->screens()) {
        createInstance(scrn);

        if (mBackgroundMode) {
            QVariantAnimation *showAnim = createAnimator(mSurfaces[scrn->name()],
                                                         mInstances[scrn->name()],
                                                         true);
            showAnim->start();
            mInstanceStates[scrn->name()] = true;
        }
    }
}

Paper::Dock::Manager::~Manager() {}

void Paper::Dock::Manager::handleMessages(QString msg, int fd)
{
    if (msg.startsWith("raise")) {
        QStringList parts = msg.split("\n");
        raiseInstance(parts[1]);

        qApp->messageClient("ack", fd);
    }

    else if (msg.startsWith("lower")) {
        QStringList parts = msg.split("\n");
        lowerInstance(parts[1]);

        qApp->messageClient("ack", fd);
    }

    else if (msg.startsWith("toggle")) {
        QStringList parts = msg.split("\n");
        if (mInstanceStates[parts[1]]) {
            hideLayerSurface(parts[1]);
        }

        else {
            showLayerSurface(parts[1]);
        }

        qApp->messageClient("ack", fd);
    }

    else if (msg.startsWith("show-menu")) {
        QStringList parts = msg.split("\n");
        toggleMenu(parts[1]);
        qApp->messageClient("ack", fd);
    }

    else {
        qWarning() << "Unhandled request:" << msg;
        qApp->messageClient("Unhandled request", fd);
    }
}

void Paper::Dock::Manager::createInstance(QScreen *scrn)
{
    Paper::Dock::UI *dock = new Paper::Dock::UI(scrn->name());

    connect(dock, &Paper::Dock::UI::toggleMenu, this, &Paper::Dock::Manager::toggleMenu);
    connect(dock, &Paper::Dock::UI::toggleWidgets, this, &Paper::Dock::Manager::toggleWidgets);

    dock->show();

    if (WQt::Utils::isWayland()) {
        /** wl_output corresponding to @screen */
        wl_output *output = WQt::Utils::wlOutputFromQScreen(scrn);

        WQt::LayerSurface *cls = wlRegistry->layerShell()->getLayerSurface(
            dock->windowHandle(), // Window Handle
            output,               // wl_output object - for multi-monitor support
            WQt::LayerShell::Top, // Background layer
            "widgets"             // Dummy namespace
        );

        /** Anchor everywhere */
        cls->setAnchors(WQt::LayerSurface::Top | WQt::LayerSurface::Bottom
                        | WQt::LayerSurface::Left);

        /** Size - when */
        cls->setSurfaceSize(dock->size());

        /** Nothing should move this */
        cls->setExclusiveZone(dock->width());

        /** No keyboard interaction */
        cls->setKeyboardInteractivity(WQt::LayerSurface::NoFocus);

        /** Default margins: hide the surface */
        cls->setMargins(QMargins(-dock->width(), 0, 0, 0));

        /** Apply the changes */
        cls->apply();

        connect(scrn, &QScreen::geometryChanged, [=]() {
            dock->setFixedHeight(scrn->size().height());
            cls->setSurfaceSize(dock->size());
            cls->apply();
        });

        mInstances[scrn->name()] = dock;
        mSurfaces[scrn->name()] = cls;
        mSurfaceStates[scrn->name()] = WQt::LayerShell::Bottom;
        mInstanceStates[scrn->name()] = false;
    }

    else {
        dock->close();
        delete dock;
    }
}

void Paper::Dock::Manager::destroyInstance(QString opName)
{
    if (mSurfaces.contains(opName)) {
        WQt::LayerSurface *surf = mSurfaces.take(opName);
        delete surf;
    }

    if (mSurfaceStates.contains(opName)) {
        mSurfaceStates.remove(opName);
    }

    if (mInstances.contains(opName)) {
        Paper::Dock::UI *w = mInstances.take(opName);
        w->close();
        delete w;
    }

    if (mInstanceStates.contains(opName)) {
        mInstanceStates.remove(opName);
    }
}

void Paper::Dock::Manager::raiseInstance(QString opName)
{
    if (not mSurfaces.contains(opName)) {
        return;
    }

    if (mSurfaceStates[opName] == WQt::LayerShell::Top) {
        return;
    }

    QVariantAnimation *hideAnim = createAnimator(mSurfaces[opName], mInstances[opName], false);
    QVariantAnimation *showAnim = createAnimator(mSurfaces[opName], mInstances[opName], true);

    connect(hideAnim, &QVariantAnimation::finished, [=]() {
        mSurfaceStates[opName] = WQt::LayerShell::Top;

        mSurfaces[opName]->setLayer(WQt::LayerShell::Top);
        mSurfaces[opName]->apply();

        mInstances[opName]->setShownAsBackground(false);
    });

    QSequentialAnimationGroup *grp = new QSequentialAnimationGroup();
    grp->addAnimation(hideAnim);
    grp->addAnimation(showAnim);

    grp->start();
    mInstanceStates[opName] = true;
}

void Paper::Dock::Manager::lowerInstance(QString opName)
{
    if (not mSurfaces.contains(opName)) {
        return;
    }

    if (mSurfaceStates[opName] == WQt::LayerShell::Bottom) {
        return;
    }

    QVariantAnimation *hideAnim = createAnimator(mSurfaces[opName], mInstances[opName], false);
    QVariantAnimation *showAnim = createAnimator(mSurfaces[opName], mInstances[opName], true);

    connect(hideAnim, &QVariantAnimation::finished, [=]() {
        mSurfaceStates[opName] = WQt::LayerShell::Bottom;

        mSurfaces[opName]->setLayer(WQt::LayerShell::Bottom);
        mSurfaces[opName]->apply();

        mInstances[opName]->setShownAsBackground(true);
    });

    QSequentialAnimationGroup *grp = new QSequentialAnimationGroup();
    grp->addAnimation(hideAnim);
    grp->addAnimation(showAnim);

    grp->start();
    mInstanceStates[opName] = true;
}

void Paper::Dock::Manager::showLayerSurface(QString opName)
{
    if (not mInstances.contains(opName)) {
        return;
    }

    mSurfaceStates[opName] = WQt::LayerShell::Top;

    mSurfaces[opName]->setLayer(WQt::LayerShell::Top);
    mSurfaces[opName]->apply();

    mInstances[opName]->setShownAsBackground(false);

    QVariantAnimation *showAnim = createAnimator(mSurfaces[opName], mInstances[opName], true);
    showAnim->start();

    mInstanceStates[opName] = true;
}

void Paper::Dock::Manager::hideLayerSurface(QString opName)
{
    QVariantAnimation *hideAnim = createAnimator(mSurfaces[opName], mInstances[opName], false);

    hideAnim->start();

    mInstanceStates[opName] = false;
}

QVariantAnimation *Paper::Dock::Manager::createAnimator(WQt::LayerSurface *surf,
                                                        Paper::Dock::UI *ui,
                                                        bool show)
{
    QVariantAnimation *anim = new QVariantAnimation();

    anim->setDuration(500);

    if (show) {
        anim->setStartValue(-ui->width());
        anim->setEndValue(0);
        anim->setEasingCurve(QEasingCurve(QEasingCurve::OutBack));
    }

    else {
        anim->setStartValue(0);
        anim->setEndValue(-ui->width());
        anim->setEasingCurve(QEasingCurve(QEasingCurve::InBack));
    }

    connect(anim, &QVariantAnimation::valueChanged, [=](QVariant val) {
        surf->setMargins(QMargins(val.toInt(), 0, 0, 0));
        surf->apply();

        qApp->processEvents();
    });

    return anim;
}

void Paper::Dock::Manager::toggleMenu(QString opName)
{
    if (widgetsVisible) {
        toggleWidgets(opName);
    }

    if (menu == nullptr) {
        QString orgName = "CuboCore";
        QString appName = "papermenu";

        /** Create the socket path */
        QString sockPath("%1/%2.socket");
        QString sockDir("%1/%2/%3");

        sockDir = sockDir.arg(QStandardPaths::writableLocation(QStandardPaths::RuntimeLocation))
                      .arg(orgName.replace(" ", "-"))
                      .arg(QString(qgetenv("XDG_SESSION_ID")));

        /** Create this folder */
        if (not QDir("/").mkpath(sockDir)) {
            return;
        }

        sockPath = sockPath.arg(sockDir).arg(appName.replace(" ", "-"));

        menu = new DFL::IPC::Client(sockPath, this);

        menu->connectToServer();
        /** Wait 100 ms for connection */
        if (not menu->waitForRegistered(100 * 1000)) {
            delete menu;
            menu = nullptr;

            return;
        }

        connect(menu, &DFL::IPC::Client::messageReceived, [=](QString msg) {
            if (msg == "shown") {
                mInstances[opName]->makeOpaque(true);
                mInstances[opName]->markMenu(true);
                menuVisible = true;
            }

            else {
                mInstances[opName]->markMenu(false);
                mInstances[opName]->makeOpaque(false);
                menuVisible = false;
            }
        });

        connect(menu, &DFL::IPC::Client::disconnected, [=]() mutable {
            delete menu;
            menu = nullptr;
            qDebug() << "Connection to PaperMenu is destroyed.";
        });
    }

    if (menu->sendMessage("toggle\n" + opName)) {
        menu->waitForReply();
    }
}

void Paper::Dock::Manager::toggleWidgets(QString opName)
{
    if (menuVisible) {
        qDebug() << "Hiding menu";
        toggleMenu(opName);
    }

    if (widgets == nullptr) {
        qDebug() << "Creating and showing widgets";

        QString orgName = "CuboCore";
        QString appName = "paperwidgets";

        /** Create the socket path */
        QString sockPath("%1/%2.socket");
        QString sockDir("%1/%2/%3");

        sockDir = sockDir.arg(QStandardPaths::writableLocation(QStandardPaths::RuntimeLocation))
                      .arg(orgName.replace(" ", "-"))
                      .arg(QString(qgetenv("XDG_SESSION_ID")));

        /** Create this folder */
        if (not QDir("/").mkpath(sockDir)) {
            return;
        }

        sockPath = sockPath.arg(sockDir).arg(appName.replace(" ", "-"));

        widgets = new DFL::IPC::Client(sockPath, this);

        widgets->connectToServer();
        /** Wait 100 ms for connection */
        if (not widgets->waitForRegistered(100 * 1000)) {
            delete widgets;
            widgets = nullptr;

            return;
        }

        connect(widgets, &DFL::IPC::Client::messageReceived, [=](QString msg) {
            if (msg == "shown") {
                mInstances[opName]->makeOpaque(true);
                mInstances[opName]->markWidgets(true);
                widgetsVisible = true;
            }

            else {
                mInstances[opName]->markWidgets(false);
                mInstances[opName]->makeOpaque(false);
                widgetsVisible = false;
            }
        });

        connect(widgets, &DFL::IPC::Client::disconnected, [=]() mutable {
            delete widgets;
            widgets = nullptr;
            qDebug() << "Connection to PaperWidgets is destroyed.";
        });
    }

    if (widgets->sendMessage("toggle\n" + opName)) {
        widgets->waitForReply();
        qDebug() << "Showing widgets";
    }
}
