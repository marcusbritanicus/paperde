# Paper Desktop
A light-weight desktop environment based on Qt/Wayland and [wayfire](https://github.com/wayfirewm/wayfire).

<img src="paperdesktop1.png" width="500">
<img src="paperdesktop2.png" width="500">
<img src="paperdesktop3.png" width="500">
<img src="paperdesktop4.png" width="500">

### Download
You can download latest release from git
* [Source](https://gitlab.com/cubocore/paper/paperdesktop)
* [ArchPackages](https://gitlab.com/cubocore/wiki/tree/master/ArchPackages)

### Dependencies:
* qt5-base
* qt5-wayland
* qt5-svg
* qt5-tools
* libdbusmenu-lxqt
* wayland
* wayland-protocols
* [CPrime](https://gitlab.com/cubocore/libcprime/)
* [CSys](https://gitlab.com/cubocore/libcsys/)
* [WayQt](https://gitlab.com/desktop-frameworks/wayqt/)
* [DFL::Applications](https://gitlab.com/desktop-frameworks/applications/)
* [DFL::IPC](https://gitlab.com/desktop-frameworks/ipc)
* [DFL::Login1](https://gitlab.com/desktop-frameworks/login1)
* [DFL::Settings](https://gitlab.com/desktop-frameworks/settings)
* [DFL::SNI](https://gitlab.com/desktop-frameworks/status-notifier/)
* [wayfire](https://github.com/WayfireWM/wayfire/) - runtime
* xdg-desktop-portal - runtime
* xdg-desktop-portal-kde - runtime
* xdg-desktop-portal-gtk - runtime
* xdg-desktop-portal-wlr - runtime
* qt5ct - runtime

#### Dependencies (Optional)
* swaylock
* swayidle
* brightnessctl - for brightness control using the keyboard brightness key
* mako - For notification
* playerctl - For keyboard media controls
* clipman - For clipboard manager for wayland

### Installation
*For building from source please take a look in the [installation - wiki](https://gitlab.com/cubocore/paper/paperdesktop/-/wikis/Installation).*

### Information
Please see the [Wiki page](https://gitlab.com/cubocore/wiki) for this info.
* Changes in latest release ([ChangeLog](https://gitlab.com/cubocore/wiki/-/blob/master/ChangeLog))
* Build from the source ([BuildInfo](https://gitlab.com/cubocore/wiki/blob/master/BuildInfo.md))
* Tested In ([Test System](https://gitlab.com/cubocore/wiki/blob/master/TestSystem))
* Known Bugs ([Current list of issues](https://gitlab.com/groups/cubocore/coreapps/-/issues))
* Help Us

### Feedback
* We need your feedback to improve the C Suite. Send us your feedback through GitLab [issues](https://gitlab.com/groups/cubocore/coreapps/-/issues).

  Or feel free to join and chat with us in IRC/Matrix #cubocore:matrix.org or [Element.io](https://app.element.io/#/room/#cubocore:matrix.org)
