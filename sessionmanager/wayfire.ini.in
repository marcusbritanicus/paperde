[alpha]
min_value = 0.100000
modifier = <alt> <super>

[animate]
close_animation = zoom
duration = 200
enabled_for = (type equals "toplevel" | (type equals "x-or" & focusable equals true))
fade_duration = 200
fade_enabled_for = type equals "overlay"
fire_color = \#B22303FF
fire_duration = 250
fire_enabled_for = none
fire_particle_size = 16.000000
fire_particles = 2000
open_animation = zoom
random_fire_color = false
startup_duration = 500
zoom_duration = 200
zoom_enabled_for = none

[annotate]
clear_workspace = <alt> <super> KEY_C
draw = <alt> <super> BTN_LEFT
from_center = true
line_width = 3.000000
method = draw
stroke_color = \#FF0000FF

[autorotate-iio]
lock_rotation = false
rotate_down = <ctrl> <super> KEY_DOWN
rotate_left = <ctrl> <super> KEY_LEFT
rotate_right = <ctrl> <super> KEY_RIGHT
rotate_up = <ctrl> <super> KEY_UP

[autostart]
01_dbus_crap = systemctl --user import-environment && dbus-update-activation-environment --systemd --all
02_xdp = /usr/libexec/xdg-desktop-portal -r
03_xdpw = /usr/libexec/xdg-desktop-portal-wlr -r
autostart_wf_shell = false
background = papershell -platform wayland
clipman-restore = clipman restore
clipman-store = wl-paste -t text --watch clipman store
gnome-keyring = gnome-keyring-daemon --daemonize --start --components=gpg,pkcs11,secrets,ssh
idle = swayidle before-sleep swaylock
libinput-gesture = libinput-gestures-setup start
notifications = mako
outputs = kanshi
polkit = /usr/bin/lxqt-policykit-agent
portal = /usr/libexec/xdg-desktop-portal
redshift = redshift -m wayland

[background-view]
app-id = mpv
command = mpv --loop=inf
file =

[bench]
average_frames = 1
frames_per_update = 3
position = top_center

[blur]
blur_by_default = type is "toplevel"
bokeh_degrade = 1
bokeh_iterations = 15
bokeh_offset = 5.000000
box_degrade = 1
box_iterations = 2
box_offset = 1.000000
gaussian_degrade = 1
gaussian_iterations = 2
gaussian_offset = 1.000000
isaturation = 1.000000
kawase_degrade = 1
kawase_iterations = 2
kawase_offset = 0.500000
method = kawase
mode = normal
saturation = 1.000000
toggle = none

[command]
binding_clipman = <super> KEY_H
binding_launcher = <super> <shift> KEY_ENTER
binding_lock = <super> <shift> KEY_ESC
binding_logout = <ctrl> <alt> KEY_DELETE
binding_media-play-pause = KEY_PLAYPAUSE
binding_media-stop = KEY_STOPCD
binding_mute = KEY_MUTE
binding_screenshot = KEY_SYSRQ | KEY_PRINT
binding_screenshot_interactive = <shift> KEY_SYSRQ | <shift> KEY_PRINT
binding_shelltasks = <super>
binding_shutdown = KEY_POWER | KEY_POWER2
binding_terminal = <super> KEY_T
command_clipman = clipman pick -t wofi
command_launcher = wofi
command_light_down = brightnessctl s 5%-
command_light_up = brightnessctl s +5%
command_lock = swaylock
command_logout = papersessionmanager --logout
command_media-next = playerctl next
command_media-play-pause = playerctl play-pause
command_media-prev = playerctl previous
command_media-stop = playerctl stop
command_mute = pulseaudio-ctl mute
command_screenshot = grim $HOME/screenshot-$(date "+%Y-%m-%d-%H:%M:%S").png && notify-send -i "camera" 'screenshot' 'saved in ~/'
command_screenshot_interactive = slurp | grim -g - $HOME/slurped-$(date "+%Y-%m-%d-%H:%M:%S").png && notify-send -i "camera" 'screenshot' 'saved in ~/'
command_shelltasks = papershell --toggleApps
command_shutdown = @UTILS_PATH@/paperlogout
command_terminal = coreterminal
command_volume_down = pulseaudio-ctl down
command_volume_up = pulseaudio-ctl up
repeatable_binding_light_down = KEY_BRIGHTNESSDOWN
repeatable_binding_light_up = KEY_BRIGHTNESSUP
repeatable_binding_media-next = KEY_NEXTSONG
repeatable_binding_media-prev = KEY_PREVIOUSSONG
repeatable_binding_volume_down = KEY_VOLUMEDOWN
repeatable_binding_volume_up = KEY_VOLUMEUP

[core]
background_color = \#1A1A1AFF
close_top_view = <super> KEY_Q | <super> KEY_W | <alt> KEY_FN_F4 | <alt> KEY_F4 | <super> BTN_MIDDLE
focus_button_with_modifiers = false
focus_buttons = BTN_LEFT | BTN_MIDDLE | BTN_RIGHT
focus_buttons_passthrough = true
max_render_time = -1
plugins = alpha animate autostart command cube decoration expo fast-switcher fisheye foreign-toplevel grid idle invert join-views move oswitch place resize switcher vswipe vswitch window-rules wm-actions wrot zoom
preferred_decoration_mode = server
transaction_timeout = 100
vheight = 1
vwidth = 1
xwayland = true

[crosshair]
line_color = \#FF0000FF
line_width = 2

[cube]
activate = <alt> <ctrl> BTN_LEFT
background = \#1A1A1AFF
background_mode = simple
cubemap_image =
deform = 0
initial_animation = 350
light = true
rotate_left = <alt> <ctrl> KEY_LEFT
rotate_right = <alt> <ctrl> KEY_RIGHT
skydome_mirror = true
skydome_texture =
speed_spin_horiz = 0.020000
speed_spin_vert = 0.020000
speed_zoom = 0.070000
zoom = 0.100000

[dbus_interface]

[dbusqt]
object = /org/DesQ/Wayfire
service = org.DesQ.Wayfire

[decoration]
active_color = \#000000DB
border_size = 1
button_order = minimize maximize close
font = Quicksand
ignore_views = none
inactive_color = \#5E5E5EDC
title_height = 27

[expo]
background = \#1A1A1AFF
duration = 300
inactive_brightness = 0.700000
keyboard_interaction = true
offset = 10
select_workspace_1 = KEY_1
select_workspace_2 = KEY_2
select_workspace_3 = KEY_3
select_workspace_4 = KEY_4
select_workspace_5 = KEY_5
select_workspace_6 = KEY_6
select_workspace_7 = KEY_7
select_workspace_8 = KEY_8
select_workspace_9 = KEY_9
toggle = <super> KEY_F10
transition_length = 200

[extra-gestures]
close_fingers = 20
move_delay = 500
move_fingers = 3

[fast-switcher]
activate = <alt> KEY_ESC
activate_backward = <alt> <shift> KEY_ESC

[fisheye]
radius = 450.000000
toggle = <ctrl> <super> KEY_F
zoom = 7.000000

[follow-focus]
change_output = true
change_view = true
focus_delay = 50
raise_on_top = true
threshold = 10

[force-fullscreen]
constrain_pointer = false
constraint_area = view
key_toggle_fullscreen = <alt> <super> KEY_F
preserve_aspect = true
transparent_behind_views = true
x_skew = 0.000000
y_skew = 0.000000

[grid]
duration = 300
restore = <super> KEY_KP0
slot_b = <super> KEY_KP2
slot_bl = <super> KEY_KP1
slot_br = <super> KEY_KP3
slot_c = <super> KEY_KP5
slot_l = <super> KEY_LEFT | <super> KEY_KP4
slot_r = <super> KEY_RIGHT | <super> KEY_KP6
slot_t = <super> KEY_KP8
slot_tl = <super> KEY_KP7
slot_tr = <super> KEY_KP9
type = simple

[hide-cursor]
hide_delay = 2000
toggle = <ctrl> <super> KEY_H

[hinge]
filename = /sys/bus/iio/devices/iio:device1/in_angl0_raw
flip_degree = 180
poll_freq = 200

[idle]
cube_max_zoom = 1.500000
cube_rotate_speed = 1.000000
cube_zoom_speed = 1000
disable_on_fullscreen = true
dpms_timeout = 300
screensaver_timeout = 3600
toggle = none

[input]
click_method = default
cursor_size = 24
cursor_theme = breeze_cursors
disable_touchpad_while_mouse = false
disable_touchpad_while_typing = true
gesture_sensitivity = 1.000000
kb_capslock_default_state = false
kb_numlock_default_state = true
kb_repeat_delay = 400
kb_repeat_rate = 40
left_handed_mode = false
middle_emulation = false
modifier_binding_timeout = 400
mouse_accel_profile = default
mouse_cursor_speed = 0.000000
mouse_scroll_speed = 1.000000
natural_scroll = false
scroll_method = default
tap_to_click = true
touchpad_accel_profile = default
touchpad_cursor_speed = 0.000000
touchpad_scroll_speed = 1.000000
xkb_layout = us
xkb_model =
xkb_options =
xkb_rules = evdev
xkb_variant =

[input-device]
output =

[invert]
preserve_hue = false
toggle = <super> KEY_I

[join-views]

[keycolor]
color = \#000000FF
opacity = 0.250000
threshold = 0.500000

[mag]
default_height = 500
toggle = <alt> <super> KEY_M
zoom_level = 75

[move]
activate = <alt> BTN_LEFT
enable_snap = true
enable_snap_off = true
join_views = false
quarter_snap_threshold = 50
snap_off_threshold = 10
snap_threshold = 10
workspace_switch_after = -1

[oswitch]
next_output = <super> KEY_O
next_output_with_win = <shift> <super> KEY_O

[output]
mode = auto
position = auto
scale = 1.000000
transform = normal

[place]
mode = center

[preserve-output]
last_output_focus_timeout = 10000

[resize]
activate = <super> BTN_RIGHT

[scale]
allow_zoom = false
bg_color = \#1A1A1AE6
duration = 500
inactive_alpha = 0.500000
interact = false
middle_click_close = false
spacing = 50
text_color = \#CCCCCCFF
title_font_size = 16
title_overlay = all
title_position = center
toggle = <super> KEY_F9 | hotspot right-top 1x1 400
toggle_all = <shift> <super> KEY_F9

[scale-title-filter]
bg_color = \#00000080
case_sensitive = false
font_size = 30
overlay = true
share_filter = false
text_color = \#CCCCCCCC

[showrepaint]
reduce_flicker = true
toggle = <alt> <super> KEY_S

[simple-tile]
animation_duration = 10
button_move = <super> BTN_LEFT
button_resize = <super> BTN_RIGHT
inner_gap_size = 5
keep_fullscreen_on_adjacent = true
key_focus_above = <super> KEY_K
key_focus_below = <super> KEY_J
key_focus_left = <super> KEY_H
key_focus_right = <super> KEY_L
key_toggle = <super> KEY_T
outer_horiz_gap_size = 0
outer_vert_gap_size = 0
tile_by_default = all

[switcher]
gesture_toggle = edge-swipe down 3
next_view = <alt> KEY_TAB
prev_view = <alt> <shift> KEY_TAB
speed = 200
touch_sensitivity = 1.000000
view_thumbnail_scale = 1.000000

[view-shot]
capture = <alt> <super> BTN_MIDDLE
filename = /tmp/snapshot.png

[vswipe]
background = \#1A1A1AFF
delta_threshold = 24.000000
duration = 180
enable_free_movement = false
enable_horizontal = true
enable_smooth_transition = true
enable_vertical = true
fingers = 4
gap = 32.000000
speed_cap = 0.050000
speed_factor = 256.000000
threshold = 0.350000

[vswitch]
background = \#1A1A1AFF
binding_1 = <super> KEY_F1
binding_2 = <super> KEY_F2
binding_3 = <super> KEY_F3
binding_4 = <super> KEY_F4
binding_down = <alt> <ctrl> KEY_DOWN
binding_left = none
binding_right = none
binding_up = <alt> <ctrl> KEY_UP
duration = 300
gap = 20
send_win_1 = <super> <shift> KEY_F1
send_win_2 = <super> <shift> KEY_F2
send_win_3 = <super> <shift> KEY_F3
send_win_4 = <super> <shift> KEY_F4
send_win_down = <alt> <shift> <super> KEY_DOWN
send_win_left = <alt> <shift> <super> KEY_LEFT
send_win_right = <alt> <shift> <super> KEY_RIGHT
send_win_up = <alt> <shift> <super> KEY_UP
with_win_down = <shift> <super> KEY_DOWN
with_win_left = <shift> <super> KEY_LEFT
with_win_right = <shift> <super> KEY_RIGHT
with_win_up = <shift> <super> KEY_UP
wraparound = true

[water]
activate = <ctrl> <super> BTN_LEFT

[windecor]
active_color = \#222222AA
attn_color = \#3CB371AA
border_size = 4
button_order = minimize maximize close
close_color = \#CC000077
font = sans-serif
font_color = \#FFFFFFFF
font_size = 12
icon_theme = breeze
ignore_views = none
inactive_color = \#333333DD
maximize_color = \#09FF0077
minimize_color = \#EDD40077
sticky_color = \#1C71D877
title_height = 24
title_position = 2
work_hard = false

[window-rules]

[winshadows]
clip_shadow_inside = true
enabled_views = type is "toplevel" & floating is true
include_undecorated_views = false
shadow_color = \#00000099
shadow_radius = 100

[winzoom]
dec_x_binding = <ctrl> <super> KEY_LEFT
dec_y_binding = <ctrl> <super> KEY_UP
inc_x_binding = <ctrl> <super> KEY_RIGHT
inc_y_binding = <ctrl> <super> KEY_DOWN
modifier = <ctrl> <super>
nearest_filtering = false
preserve_aspect = true
zoom_step = 0.100000

[wm-actions]
minimize = <alt> KEY_F9
send_to_back = none
toggle_always_on_top = <shift> <super> KEY_T
toggle_fullscreen = none
toggle_maximize = <alt> KEY_F10
toggle_showdesktop = <super> KEY_D
toggle_sticky = <shift> <super> KEY_S

[wobbly]
friction = 3.000000
grid_resolution = 6
spring_k = 8.000000

[workarounds]
all_dialogs_modal = true
app_id_mode = stock
dynamic_repaint_delay = false
use_external_output_configuration = false

[workspace-names]
background_color = \#333333B3
display_duration = 500
font = sans-serif
position = center
show_option_names = false
text_color = \#FFFFFFFF

[wrot]
activate = <ctrl> <super> BTN_RIGHT
activate-3d = <shift> <super> BTN_RIGHT
invert = false
reset = <ctrl> <super> KEY_R
reset-one = <super> KEY_R
reset_radius = 25.000000
sensitivity = 24

[zoom]
interpolation_method = 0
modifier = <super>
smoothing_duration = 300
speed = 0.010000
