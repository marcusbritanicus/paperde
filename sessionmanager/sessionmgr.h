/*
  *
  * This file is a part of PaperSessionManager.
  * PaperSessionManager is the Session Manager for PaperDesktop
  * Copyright 2020 CuboCore Group
  *
  *
  *
  * This file was originally a part of DesQ project (https://gitlab.com/desq/)
  * Suitable modifications have been done to meet the needs of PaperDesktop.
  *
  *
  *
  * This program is free software; you can redistribute it and/or modify
  * it under the terms of the GNU General Public License as published by
  * the Free Software Foundation; either version 3 of the License, or
  * (at your option) any later version.
  *
  *
  *
  * This program is distributed in the hope that it will be useful,
  * but WITHOUT ANY WARRANTY; without even the implied warranty of
  * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  * GNU General Public License for more details.
  *
  *
  *
  * You should have received a copy of the GNU General Public License
  * along with this program; if not, write to the Free Software
  * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
  * MA 02110-1301, USA.
  *
  */

#pragma once

#include <QProcess>
#include <QString>

class QSettings;
class SessionAdaptor;
class PaperAdaptor;

namespace DFL {
class Login1;
}

class sessionmanager : public QObject
{
    Q_OBJECT

public:
    /* Init */
    explicit sessionmanager();

public Q_SLOTS:
    /* Start the session by performing various tasks */
    void startSession();

    /* Stop the session by saving the state */
    void stop();

    /* Intitiate quit */
    void handleMessages(const QString &, int fd);

private:
    /* Setup the global environmental vriables like SESSION, etc */
    void setupEnvironmentVars();

    /* Start the desktoop shell */
    void startWayfire();

    /* QSettings instance */
    QSettings *session;

    /* Wayfire's pid, and config file */
    QProcess *wfProc;
    QString wfConfig;
    qint64 wfPID;
    int wfRestartCount;

    /* Login1 */
    DFL::Login1 *login1;

    /* DBus Adaptors */
    SessionAdaptor *pwr;
    PaperAdaptor *paper;

protected:
    void run();
};
