/*
  *
  * This file is a part of PaperSessionManager.
  * PaperSessionManager is the Session Manager for paperdesktop
  * Copyright 2020 CuboCore Group
  *
  *
  *
  * This file was originally a part of DesQ project (https://gitlab.com/desq/)
  * Suitable modifications have been done to meet the needs of paperdesktop.
  *
  *
  *
  * This program is free software; you can redistribute it and/or modify
  * it under the terms of the GNU General Public License as published by
  * the Free Software Foundation; either version 3 of the License, or
  * (at your option) any later version.
  *
  *
  *
  * This program is distributed in the hope that it will be useful,
  * but WITHOUT ANY WARRANTY; without even the implied warranty of
  * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  * GNU General Public License for more details.
  *
  *
  *
  * You should have received a copy of the GNU General Public License
  * along with this program; if not, write to the Free Software
  * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
  * MA 02110-1301, USA.
  *
  */

#include "sessionadaptor.h"
#include <QtCore/QByteArray>
#include <QtCore/QList>
#include <QtCore/QMap>
#include <QtCore/QMetaObject>
#include <QtCore/QString>
#include <QtCore/QStringList>
#include <QtCore/QVariant>

#include <csys/power.h>

SessionAdaptor::SessionAdaptor(QObject *parent)
    : QDBusAbstractAdaptor(parent)
{
    setAutoRelaySignals(true);
    pwr = new Power(this);
}

SessionAdaptor::~SessionAdaptor() {}

void SessionAdaptor::Hibernate()
{
    QMetaObject::invokeMethod(parent(), "handleMessages", Q_ARG(QString, "hibernate"));
}

void SessionAdaptor::Logout()
{
    QMetaObject::invokeMethod(parent(), "handleMessages", Q_ARG(QString, "logout"));
}

void SessionAdaptor::Reboot()
{
    QMetaObject::invokeMethod(parent(), "handleMessages", Q_ARG(QString, "reboot"));
}

void SessionAdaptor::Shutdown()
{
    QMetaObject::invokeMethod(parent(), "handleMessages", Q_ARG(QString, "shutdown"));
}

void SessionAdaptor::Suspend()
{
    QMetaObject::invokeMethod(parent(), "handleMessages", Q_ARG(QString, "suspend"));
}

bool SessionAdaptor::CanSuspend()
{
    return pwr->systemCanSuspend();
}

bool SessionAdaptor::CanHibernate()
{
    return pwr->systemCanHibernate();
}

bool SessionAdaptor::CanShutdown()
{
    return pwr->systemCanHalt();
}

bool SessionAdaptor::CanReboot()
{
    return pwr->systemCanReboot();
}

PaperAdaptor::PaperAdaptor(QObject *parent)
    : QDBusAbstractAdaptor(parent)
{
    setAutoRelaySignals(true);
}

PaperAdaptor::~PaperAdaptor() {}

void PaperAdaptor::RegisterService(QString service)
{
    services << service;
}

QStringList PaperAdaptor::RegisteredServices()
{
    return services;
}
