/**
  * This file is a part of PaperShell.
  * PaperShell is the Desktop Shell App for Paper Desktop
  * Copyright 2020 CuboCore Group
  *
  * This file was originally a part of DesQ project (https://gitlab.com/desq/)
  * Suitable modifications have been done to meet the needs of PaperDesktop.
  *
  * This program is free software; you can redistribute it and/or modify
  * it under the terms of the GNU General Public License as published by
  * the Free Software Foundation; either version 3 of the License, or
  * (at your option) any later version.
  *
  * This program is distributed in the hope that it will be useful,
  * but WITHOUT ANY WARRANTY; without even the implied warranty of
  * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  * GNU General Public License for more details.
  *
  * You should have received a copy of the GNU General Public License
  * along with this program; if not, write to the Free Software
  * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
  * MA 02110-1301, USA.
  **/

#include <DFCoreApplication.hpp>
#include <cprime/systemxdg.h>
#include <QDir>

#include "sessionmgr.h"
#include <paper-config.h>
#include <paperde/paperlog.h>

void houseKeeping()
{
    /**
	  * This function will check if all the necessary folders exist.
	  * Typically, we need ~/.config/paperde, ~/.cache/paperde.
	  * We will use QDir::mkpath(...), which does not complain if the
	  * directories exist.
	  *
	  * Currently, we rely on qt5ct to provide Qt theming. We need to
	  * provide a good default config. Also, fontconfig
	  *
	  * Based on other needs, eventually, we can expand this function
	  * to perform more work.
	  */

    QDir cache(CPrime::SystemXdg::userDir(CPrime::SystemXdg::XDG_CACHE_HOME));

    cache.mkpath("paperde");

    QDir config(CPrime::SystemXdg::userDir(CPrime::SystemXdg::XDG_CONFIG_HOME));

    config.mkpath("paperde");

    /** For fontconfig/fonts.conf */
    config.mkpath("fontconfig");

    /** For Qt5CT: qt5ct/qt5ct.conf. qt5ct/colors/Dark.conf */
    config.mkpath("qt5ct/colors/");

    /** We can blindly do this: QFile::copy will not over-write existing files. */
    QFile::copy(ConfigPath "fonts.conf", config.filePath("fontconfig/fonts.conf"));
    QFile::copy(ConfigPath "qt5ct/qt5ct.conf", config.filePath("qt5ct/qt5ct.conf"));
    QFile::copy(ConfigPath "qt5ct/colors/Dark.conf", config.filePath("qt5ct/colors/Dark.conf"));
}

int main(int argc, char *argv[])
{
    houseKeeping();

    QDir cache(CPrime::SystemXdg::userDir(CPrime::SystemXdg::XDG_CACHE_HOME));

    Paper::log = fopen(cache.filePath("paperde/PaperSession.log").toLocal8Bit().data(), "a");

    qInstallMessageHandler(Paper::Logger);

    qDebug() << "------------------------------------------------------------------------";
    qDebug() << "Paper Session started at"
             << QDateTime::currentDateTime().toString("yyyyMMddThhmmss").toUtf8().constData();
    qDebug() << "------------------------------------------------------------------------";

    DFL::CoreApplication app(argc, argv);

    // Set application info
    app.setOrganizationName("CuboCore");
    app.setApplicationName(APPNAME);
    app.setApplicationVersion("v" PROJECT_VERSION);

    if (app.lockApplication()) {
        if (argc > 2) {
            qCritical() << "No arguments can be used while session is not running.";
            return 1;
        }

        qDebug() << "Paper Session started";

        sessionmanager *session = new sessionmanager();

        QObject::connect(&app,
                         &DFL::CoreApplication::messageFromClient,
                         session,
                         &sessionmanager::handleMessages);
        session->startSession();

        return app.exec();
    }

    else {
        if (argc < 2) {
            qWarning() << "Session already running";
            return 0;
        }
        /* Quit the session: LOGOUT */
        if (argv[1] == QStringLiteral("--logout")) {
            qInfo() << "Trying to quit" << app.messageServer("logout");
            return 0;
        } else if (argv[1] == QStringLiteral("--suspend")) {
            qInfo() << "Trying to shutdown" << app.messageServer("suspend");
            return 0;
        } else if (argv[1] == QStringLiteral("--hibernate")) {
            qInfo() << "Trying to shutdown" << app.messageServer("hibernate");
            return 0;
        } else if (argv[1] == QStringLiteral("--shutdown")) {
            qInfo() << "Trying to shutdown" << app.messageServer("shutdown");
            return 0;
        } else if (argv[1] == QStringLiteral("--reboot")) {
            qInfo() << "Trying to reboot" << app.messageServer("reboot");
            return 0;
        }
        /* Open a specific session */
        else if (argv[1] == QStringLiteral("--session")) {
            QStringList args = app.arguments();
            args.pop_front();
            qInfo() << "Trying to open specific session";
            app.messageServer(args.join(" "));
        } else {
            qCritical() << "Invalid arguments.";
            return 1;
        }
    }

    return 0;
}
