project(
	'PaperDE',
	'c',
	'cpp',
	version: '0.2.2',
	license: 'GPLv3',
	meson_version: '>=0.59.0',
	default_options: [
		'cpp_std=c++17',
		'c_std=c11',
		'warning_level=2',
		'werror=false',
	],
)

add_global_arguments( '-DPROJECT_VERSION="@0@"'.format( meson.project_version() ), language : 'cpp' )
add_project_link_arguments( ['-rdynamic','-fPIC'], language:'cpp' )

# Qt6 Module, for moc, and uic
Qt6 = import( 'qt6' )

# Qt6 dependencies
Qt6Core	      = dependency( 'qt6', modules: [ 'Core' ] )
Qt6Gui	      = dependency( 'qt6', modules: [ 'Gui' ] )
Qt6Widgets    = dependency( 'qt6', modules: [ 'Widgets' ] )
Qt6DBus	      = dependency( 'qt6', modules: [ 'DBus' ] )
Qt6Network    = dependency( 'qt6', modules: [ 'Network' ] )
Qt6UiTools    = dependency( 'qt6', modules: [ 'UiTools' ] )

# Qt6 DBusMenu
DBusMenu      = dependency( 'dbusmenu-lxqt' )

# CPrime dependencies
CPrimeCore    = dependency( 'cprime-core' )
CPrimeGui     = dependency( 'cprime-gui' )
CPrimeWidgets = dependency( 'cprime-widgets' )


# CPrime dependency
CSys          = dependency( 'csys' )

# WayQt
WayQt         = dependency( 'wayqt-qt6' )

# DFL
DFSNI         = dependency( 'df6sni' )
DFIpc         = dependency( 'df6ipc' )
DFCoreApp     = dependency( 'df6coreapplication' )
DFApp         = dependency( 'df6application' )
DFLogin1      = dependency( 'df6login1' )

conf_data = configuration_data()
conf_data.set( 'version', meson.project_version() )
conf_data.set( 'INSTALL_PREFIX', get_option( 'prefix' ) )
conf_data.set( 'SHARED_DATA_PATH', join_paths( get_option( 'prefix' ), get_option( 'datadir' ), 'paperde' ) )
conf_data.set( 'PLUGIN_PATH', join_paths( get_option( 'prefix' ), get_option( 'libdir' ), 'paperde', 'plugins' ) )
conf_data.set( 'UTILS_PATH', join_paths( get_option( 'prefix' ), get_option( 'libdir' ), get_option( 'libexecdir' ), 'paperde' ) )

# Configure this so that the whole project can access it
configure_file(
	input : 'libpaperde/core/config.h.in',
	output : 'paper-config.h',
	install: true,
	install_dir: join_paths( 'include', 'paperde' ),
	configuration : conf_data
)

configure_file(
	input : 'sessionmanager/wayfire.ini.in',
	output : 'wayfire.ini',
	install: true,
	install_dir: join_paths( get_option( 'prefix' ), get_option( 'datadir' ), 'paperde' ),
	configuration : conf_data
)

config_inc = include_directories('.')


ShellInc = include_directories('papershell')

# Our main library for use across the entire Paper project
subdir( 'libpaperde' )

# Main UI
subdir( 'papershell' )

# Session Manager that starts wayfire and, through it, papershell
subdir( 'sessionmanager' )

# Settings GUI
subdir( 'settings' )

install_subdir(
	'resources/qt5ct',
	install_dir: join_paths( conf_data.get( 'SHARED_DATA_PATH' ), 'configs' )
)

install_data(
	'resources/fonts.conf',
	install_dir: join_paths( conf_data.get( 'SHARED_DATA_PATH' ), 'configs' )
)

install_data(
	'papershell/shell/papershell.conf',
	install_dir: join_paths( conf_data.get( 'SHARED_DATA_PATH' ), 'configs' )
)
